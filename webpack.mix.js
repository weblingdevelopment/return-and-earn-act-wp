let path = require('path');
let merge = require('webpack-merge');
let mix = require('laravel-mix');

/*
  | Setup up the distribution path 
*/
mix.setPublicPath('./');


/*
  | Setup mix config for individual mix tasks
*/
mix.customConfigs = [];
mix.customOptions = [];


/* 
  | all the tasks for the project goes here
*/

/*
 | initialize twig -> html  
 */
let templates = require('./_mix-tasks/templates');
templates.init();


/*
| initialize sass -> css
*/
let sass = require('./_mix-tasks/sass');
sass.init();

/*
 | usePostCss plugins
 */
let postCss = require('./_mix-tasks/postCss');
postCss.init();





/*
  | copy images
  */
let images = require('./_mix-tasks/images');
images.init();


/**
 * copy template twig to wordpress folder
*/
// let copyTemplate = require('./_mix-tasks/copyTemplates');
// copyTemplate.init();

/*
| copy fonts
*/
// let fonts = require('./_mix-tasks/fonts');
// fonts.init();



/*
  | JS Autoload 
  | expose libraries as global to every module that webpack encounters
*/

mix.autoload({
  jquery: ['jQuery', '$', 'jquery']
});

/*
  | initialize twig -> Scripts  
*/
let scripts = require('./_mix-tasks/scripts');
scripts.init();

/*
 | Browser Sync
 */
let browserSync = require('./_mix-tasks/browserSync');
browserSync.init();



/*
  Source map while developing
*/

mix.customConfigs.push({
  devtool: 'source-map'
});

mix.sourceMaps();


mix.disableNotifications();



/* 
|  Merge all custom webpack config & options from individual tasks
*/

/*
| Pass all custom webpack options 
*/
mix.options(merge(...mix.customOptions))

/*
| Pass all custom webpack configs 
*/
mix.webpackConfig(merge(...mix.customConfigs));



