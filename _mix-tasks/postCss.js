let path = require('path');
let mix = require('laravel-mix');

let init = function () {


  // autoprefixer options
  mix.customOptions.push({
    postCss: [
      require('autoprefixer')({
          browsers: ['last 3 versions'],
          cascade: false
      })
    ]
  });

};

module.exports = {
  init 
}
