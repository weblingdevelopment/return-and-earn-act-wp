let path = require('path');
let mix = require('laravel-mix');

let init = function () {

  /*
  | copy fonts 
  */
  mix.copyDirectory('src/fonts', 'public/fonts')

};

module.exports = {
  init 
}
