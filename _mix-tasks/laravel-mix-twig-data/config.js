'use strict';
/*
 |----------------------------------------------------------------
 | Twig JS - https://github.com/twigjs/twig.js
 |----------------------------------------------------------------
 |
 */
let config = {
    search: '**/*.{data.json,twig}',
    ignore: ['**/node_modules/**/*'],
    pretty: true
};

module.exports = config;
