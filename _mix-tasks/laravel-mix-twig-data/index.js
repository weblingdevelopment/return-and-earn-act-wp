
const _ = require('lodash');
const glob = require('glob');
const path = require('path');
const config = require('./config');
const Verify = require('laravel-mix/src/Verify');

const MixTwigDataTask = require('./MixTwigDataTask');

/**
 *  main entrance
 *
 * @param src
 * @param output
 * @param pluginOptions
 * @returns {twig}
 */
var twigData = function (src, output, pluginOptions = {}) {
    
  let options = _.merge(config, pluginOptions);

  Verify.dependency('twig', ['twig'], true);

  let files = glob.sync(path.join(src, options.search), {ignore: options.ignore});
  let task = new MixTwigDataTask({ src, output, options, files });

  Mix.addTask(task);

  return this;

}

module.exports = twigData;

