
const Task = require('laravel-mix/src/tasks/Task');
const File = require('laravel-mix/src/File');
const FileCollection = require('laravel-mix/src/FileCollection');

const notifier = require('node-notifier');
const mkdirp = require('mkdirp');
const glob = require('glob');
const path = require('path');

var Twig = require('twig'); // Twig module
const twig = Twig.twig;       // Render function

const fs   = require('fs');


const MixTwigTask = ('../laravel-mix-twig/MixTwigTask');

class MixTwigDataTask extends Task {

  /**
   * Run the twig compiler.
   */
  run() {
    
    let {src, output, options, files} = this.data;

    this.src = src;
    this.dest = output;
    this.config = options;
    this.templates = files;

    this.files = new FileCollection(
        glob.sync(path.join(src, options.search), {ignore: ['**/node_modules/**/*']})
    );
      
  }

  /**
   * Compiles a collection of twig templates.
   *
   */
  compile() {
   // console.log(Mix.tasks);
  }

  /**
   * Compiles a single twig template
   *
   * @param {string} src Path to the twig source file
   * @param {number} index
   * @param {string} config 
   */
  compileTemplate(src, index, config) {
  }

  /**
   * Recompile on change when using watch
   *
   * @param {string} updatedFile
   */
  onChange(updatedFile) {
    Mix.dispatch('twig-data-updated');
  }


  /**
   * Handle successful compilation.
   *
   * @param {string} output
   */
  onSuccess(output) {
      if (Config.notifications.onSuccess) {
          notifier.notify({
              title: 'Laravel Mix',
              message: 'twig Compilation Successful',
              //icon: Mix.paths.root('node_modules/laravel-mix-blade-twig/src/logo.png')
          });
      }
  }


  /**
   * Handle failed compilation.
   *
   * @param {string} output
   */
  onFail(output) {
      console.log("\n");
      console.log('twig Compilation Failed!');
      console.log();
      console.log(output);

      if (Mix.isUsing('notifications')) {
          notifier.notify({
              title: 'Laravel Mix',
              subtitle: 'twig Compilation Failed',
              message: output,
              //icon: Mix.paths.root('node_modules/laravel-mix-blade-twig/src/logo.png')
          });
      }
  }

}

module.exports = MixTwigDataTask;
