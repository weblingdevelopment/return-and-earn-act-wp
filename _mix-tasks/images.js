/*
| - https://github.com/Klathmon/imagemin-webpack-plugin
| - If you are using ImageMin plugin add additional plugins whne necessary
*/

let path = require('path');
let mix = require('laravel-mix');

//let ImageminPlugin = require('imagemin-webpack-plugin').default;
//let CopyWebpackPlugin = require('copy-webpack-plugin');



let init = function () {

  /*
  | optmize images - will slow down the build process
  | can update webpackConfig and add image optization plugins if needed
  */

  /*
  mix.customConfigs.push({
      plugins: [
        new CopyWebpackPlugin([{
            from: './src/images',
            to: 'images', // Laravel mix will place this in 'public/img'
        }]),
        new ImageminPlugin({
          pngquant: {
            quality: '20'
          }
        })
      ]
  });
  */

  /*
  | you can use optmize images using tools like : https://imageoptim.com/mac & just copy image directory
  | then copy the images over 
  */

  /**
   * Define production and dev 
   */
  if (mix.inProduction()) {
    imageOutputDir = 'WP/wp-content/themes/webling-act/static/images';
    jsonOutputDir = 'WP/wp-content/themes/webling-act/static/js';
  } else {
    imageOutputDir = 'public/static/images';
    jsonOutputDir = 'public/';
  }
  mix
  .copyDirectory('FE-Build/src/images', imageOutputDir);

  mix
  .copyDirectory('FE-Build/src/data.json', jsonOutputDir);
  // For FE work Only
  mix
  .copyDirectory('FE-Build/src/images', 'public/wp-content/themes/webling-act/static/images');

};

module.exports = {
  init 
}
