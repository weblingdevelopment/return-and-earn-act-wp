let path = require('path');
let mix = require('laravel-mix');

// twig templates
mix.twig = require('./laravel-mix-twig');
mix.twigData = require('./laravel-mix-twig-data');


let init = function () {

  let bust = process.argv.includes('--env.bust') || false;

  mix.twig(
      'WP/wp-content/themes/webling-act/templates/pages', 'public/', 
      {
        'global_data_file_path': 'WP/wp-content/themes/webling-act/templates/global.data.json',
        'bust': bust
      }
  );

  // mix.twigData('FE-Build/src/templates', '');
  mix.twigData('WP/wp-content/themes/webling-act/templates', '');
};

module.exports = {
  init
}
