let path = require('path');
let mix = require('laravel-mix');

let init = function () {

    if (mix.inProduction()) {
      cssOutputDir = 'WP/wp-content/themes/webling-act/static/css';
    } else {
      cssOutputDir = 'public/static/css';      
    }
    mix
    .sass('FE-Build/src/scss/main.scss', cssOutputDir)
    .options({ processCssUrls: false });
    
};

module.exports = {
  init
}
