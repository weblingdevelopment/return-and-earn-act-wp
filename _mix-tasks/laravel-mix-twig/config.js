'use strict';
/*
 |----------------------------------------------------------------
 | Twig JS - https://github.com/twigjs/twig.js
 | Just used for watch (updates to *.twig & *.data.json files)
 | re-compiles twig templates
 |----------------------------------------------------------------
 |
 */
let config = {
    search: '**/*.twig',
    ignore: ['**/node_modules/**/*'],
    pretty: true
};

module.exports = config;
