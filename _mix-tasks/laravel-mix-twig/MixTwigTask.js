
const Task = require('laravel-mix/src/tasks/Task');
const File = require('laravel-mix/src/File');
const FileCollection = require('laravel-mix/src/FileCollection');

const notifier = require('node-notifier');
const mkdirp = require('mkdirp');
const glob = require('glob');
const path = require('path');

var Twig = require('twig'); // Twig module
Twig.cache(false)
const twig = Twig.twig;  // Render function

const fs = require('fs');

class MixTwigTask extends Task {

    /**
     * Run the twig compiler.
     */
    run() {

        let { src, output, options, files } = this.data;

        this.src = src;
        this.dest = output;
        this.config = options;
        this.templates = files;

        this.files = new FileCollection(
            glob.sync(path.join(src, options.search), { ignore: ['**/node_modules/**/*'] })
        );


        //Preprare destination assets
        this.assets = files.map(asset => this.prepareAssets(asset));

        this.compile();

        Mix.listen('twig-data-updated', () => this.compile());
    }

    /**
     * Compiles a collection of twig templates.
     *
     */
    compile() {
        try {
            this.templates.forEach((template, index, config) => this.compileTemplate(template, index, this.config));

            this.onSuccess();

        } catch (e) {
            this.onFail(e.name + ': ' + e.message);
        }

        return this;
    }

    /**
     * Compiles a single twig template
     *
     * @param {string} src Path to the twig source file
     * @param {number} index
     * @param {string} config 
     */
    compileTemplate(src, index, config) {
        let file = new File(src);
        let output = this.assets[index];


        Twig.extendFunction("mix", function (value) {
            if (config.bust) {
                let hash = file.version();
                return value + '?id=' + hash;
            } else {
                return value;
            }
        });

        try {

            // global data
            var globalDataFile = config.global_data_file_path;
            var globalData = JSON.parse(fs.readFileSync(globalDataFile, 'utf8'));
            //console.log(globalData);

            // page specific data file
            var pageDataFile = file.path().replace('.twig', '.data.json');
            var pageData = JSON.parse(fs.readFileSync(pageDataFile, 'utf8'));
            //console.log(pageData);

            // global & page specific data merged
            let merge = require('lodash').assign;
            var mergedData = merge(globalData, pageData);

            var html = twig({
                //allowInlineIncludes: true,
                path: './WP/wp-content/themes/webling-act/templates/',
                data: fs.readFileSync(file.path(), 'utf8'),
                namespaces: { 'root': './WP/wp-content/themes/webling-act/templates' }
                // namespaces: { 'root': './src/templates/' }
            }).render(mergedData);

            if (!fs.existsSync(output.base())) {
                mkdirp.sync(output.base());
            }

            fs.writeFileSync(output.path(), html);


        } catch (e) {
            throw e;
        }
    }

    /**
     * Recompile on change when using watch
     *
     * @param {string} updatedFile
     */
    onChange(updatedFile) {
        this.compile();
    }


    /**
     * Handle successful compilation.
     *
     * @param {string} output
     */
    onSuccess(output) {
        if (Config.notifications.onSuccess) {
            notifier.notify({
                title: 'Laravel Mix',
                message: 'twig Compilation Successful',
                icon: Mix.paths.root('node_modules/laravel-mix-blade-twig/src/logo.png')
            });
        }
    }


    /**
     * Handle failed compilation.
     *
     * @param {string} output
     */
    onFail(output) {
        console.log("\n");
        console.log('twig Compilation Failed!');
        console.log();
        console.log(output);

        if (Mix.isUsing('notifications')) {
            notifier.notify({
                title: 'Laravel Mix',
                subtitle: 'twig Compilation Failed',
                message: output,
                //icon: Mix.paths.root('node_modules/laravel-mix-blade-twig/src/logo.png')
            });
        }
    }

    prepareAssets(src) {
        var file = new File(src);
        var folder = file.filePath.replace(this.src, '').replace(file.name(), '');
        var output = path.join(this.dest, folder, file.nameWithoutExtension() + '.html');
        var asset = new File(output);
        Mix.addAsset(asset);
        return asset;
    }
}

module.exports = MixTwigTask;
