let path = require('path');
let mix = require('laravel-mix');

let init = function () {

  /*
  | 
  */

 mix.autoload(['jquery']);

  if (mix.inProduction()) {
    mix.js([
      'FE-Build/src/js/bootstrap.js',
      'FE-Build/src/js/vendors/jquery-accessible-accordion-aria.js',
      'FE-Build/src/js/vendors/jquery-ui.js',
      'FE-Build/src/js/vendors/jquery.slick.js',
        'FE-Build/src/js/vendors/jquery.overlayScrollbars.min.js',
    ], 'WP/wp-content/themes/webling-act/static/js/vendors.js');
    mix.scripts([
      'FE-Build/src/js/vendors/infobox.js'
    ], 'WP/wp-content/themes/webling-act/static/js/infobox.js');
    mix.js([
      'FE-Build/src/js/scripts.js',
    ], 'WP/wp-content/themes/webling-act/static/js/scripts.js');
  } else {
    mix.js([
      'FE-Build/src/js/bootstrap.js',
      'FE-Build/src/js/vendors/jquery-accessible-accordion-aria.js',
      'FE-Build/src/js/vendors/jquery-ui.js',
      'FE-Build/src/js/vendors/jquery.slick.js',
      'FE-Build/src/js/vendors/jquery.overlayScrollbars.min.js',
    ], './public/static/js/vendors.js');
    mix.scripts([
      'FE-Build/src/js/vendors/infobox.js'
    ], './public/static/js/infobox.js');
    mix.js([
      'FE-Build/src/js/scripts.js'
    ], './public/static/js/scripts.js');
  }

};

module.exports = {
  init 
};
