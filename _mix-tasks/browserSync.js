let path = require('path');
let mix = require('laravel-mix');

let init = function () {

  /*
  | 
  */

  mix.browserSync({
    watchOptions: {
      usePolling: false,
      watchEvents: ["change", "add", "addDir"]
    },
    files: [
      'public/**/*.html',
      'public/static/js/**/*.js',
      'public/static/css/**/*.css',
      'src/**/*.data.json'
    ],
    proxy: false,
    server: {
      baseDir: "./public/",
      index: "master.html"
    }
  })

};

module.exports = {
  init
}
