/**
 * @param data
 * This function create list view with restrict distance and only show 9 items
 */
const createMapView = (function (data) {

    //Remove scroll CSS from tag html
    $('html').css('overflow-y', 'hidden');

    //Slider
    let selectorSlider = '.c-return-point-card__items';
    let isSliderLoaded = false;

    //Initialize scroll bar
    function initializeScrollBar(){

        $('.c-return-point-result--list-view').overlayScrollbars({
            className : "os-theme-dark",
            nativeScrollbarsOverlaid : {
                initialize : ($(window).outerWidth() > 767)
            },
        });
    }

    //Flag to indicate google maps is loaded
    let googleMapsLoaded = false;

    //Initialize map
    let mapData = data;
    initialMap();
    function initialMap(){
        let map = new google.maps.Map(document.getElementById('c-results-map'), {
            center: new google.maps.LatLng(-33.85942, 151.0702),
            streetViewControl: false,
            fullscreenControl: false,
            mapTypeControl: false,

            //Hide point of interest and businesses
            styles: [
                {
                    "featureType": "poi",
                    "elementType": "labels",
                    "stylers": [{"visibility": "off"}]
                },
                {
                    featureType: 'poi.business',
                    stylers: [{visibility: 'off'}]
                },
            ],
        });

        // Create marker
        createMarker(map, mapData);
    }

    function createMarker(map, data){
        let currentURL = window.location.origin;
        let marker;
        let activeMarker;
        let iconDefault = '';
        let iconSelected = '';
        let bounds = new google.maps.LatLngBounds();

        //Iterate all return points
        let markers = data.map(function(location, i){

            //If return point is express
            if(location.return_point_type.toLocaleLowerCase().indexOf('express') > -1 ){
                iconDefault = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/icon-map-express.png';
                iconSelected = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/icon-map-express-active.png';
            }else{
                iconDefault = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/icon-map-bd.png';
                iconSelected = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/icon-map-bd-active.png';
            }

            //Default icon settings
            let defaultIconSetting = {
                // This marker is 40 pixels wide by 62 pixels high.
                scaledSize: new google.maps.Size(40, 62),
            };

            //Default icon
            let iconDefaultType = Object.assign({
                url: iconDefault,
            }, defaultIconSetting);

            //Selected icon
            let iconSelectedType = Object.assign({
                url: iconSelected,
            }, defaultIconSetting);

            //Initiate map
            let latLng = new google.maps.LatLng(location.latitude, location.longitude);
            let marker = new google.maps.Marker({
                position: latLng,
                map: map,
                icon: iconDefaultType,
                returnPointId: location.id,
                defaultIcon: iconDefaultType,
                zIndex: 100
            });

            //Click event handler
            google.maps.event.addListener(marker, 'click', function(e) {

                //Check active card
                checkActiveCard (marker.returnPointId);

                // Move to related return point when custom marker is clicked
                if ($(window).outerWidth() < 768) {

                    //If slick has initialized
                    if(isSliderLoaded) {
                        $(selectorSlider).slick("slickGoTo", i);
                    }

                    //Set marker position in center
                    map.setCenter(latLng);
                } else {

                    //If marker in not in bounds
                    if (!map.getBounds().contains(marker.getPosition())) {

                        //Set marker position in center
                        map.setCenter(latLng);
                    }
                }

                //Check to see if activeMarker is set
                //If so, set the icon back to the default
                activeMarker && activeMarker.setIcon(activeMarker.defaultIcon);
                activeMarker && activeMarker.setZIndex(100);

                //Set the icon for the clicked marker
                marker.setIcon(iconSelectedType);
                marker.setZIndex(120);

                //Update the value of activeMarker
                activeMarker = marker;
            });

            // To show all return points on map
            bounds.extend(marker.getPosition());
            map.fitBounds(bounds);

            return marker;
        });

        //On slider swipe
        $(selectorSlider).on('init.slick', function(){
            google.maps.event.trigger(markers[0], 'click');
            checkActiveCard (markers[0].returnPointId);
            isSliderLoaded = true;
        });

        //Tiles loaded handler
        google.maps.event.addListener(map, 'tilesloaded', function() {

            //Set flag to true
            googleMapsLoaded = true;

            $('a.c-return-point-card--link').click(function(){

                //Call marker click handler
                let idx = $('a.c-return-point-card--link').index(this);
                google.maps.event.trigger(markers[idx], 'click');
                checkActiveCard (markers[idx].returnPointId);
            });

            //Clear tiles loaded function
            google.maps.event.clearListeners(map, 'tilesloaded');
        });

        //Load resize handler
        $(window).on('load resize', function() {

            //If window width is greater than 767
            if ($(window).outerWidth() > 767) {
                $('a.c-return-point-card--link').click(function(){

                    //Call marker click handler
                    let idx = $('a.c-return-point-card--link').index(this);
                    google.maps.event.trigger(markers[idx], 'click');
                    checkActiveCard (markers[idx].returnPointId);
                });
            }
        });
    }

    //Check active card
    function checkActiveCard (id){

        //Vars
        let currentURL = window.location.origin;
        let baseUrl = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/';

        //Return points defaults
        let returnPointsType = {
            express: {
                iconDefault:  'icon-express-colored.png',
                iconSelected: 'icon-express-selected.png',
            },
            bulk: {
                iconDefault:  'icon-bd-colored.png',
                iconSelected: 'icon-bd-selected.png',
            }
        };

        //Iterate all cards
        $('a.c-return-point-card--link').each(function(){

            //Removing selected class
            $(this).parent().removeClass('selected-return-point');

            //Checking if card is express
            let isExpress = $(this).children().hasClass('express');

            //Replacable default icon
            let replaceDefaultIcon = baseUrl + (isExpress ? returnPointsType['express']['iconDefault'] : returnPointsType['bulk']['iconDefault']);

            //Set card's icon to default
            $(this).children().children('img').attr('src', replaceDefaultIcon);

            //If element has relevant return point ID
            if($(this).attr('data-id') == id){

                //Add selected class
                $(this).parent().addClass('selected-return-point');

                //Replacable default icon
                let replaceSelectedIcon = baseUrl + (isExpress ? returnPointsType['express']['iconSelected'] : returnPointsType['bulk']['iconSelected']);

                //Set card's icon to selected
                $(this).children().children('img').attr('src', replaceSelectedIcon);

                //If window width is greater than 767
                if ($(window).outerWidth() > 767) {

                    //If card is in view
                    if(gambitGalleryIsInView(this) == false){

                        //Scroll target height
                        let targetTop = $(this).offset().top - 134;

                        //Scroll to element
                        $('.c-return-point-result--list-view').overlayScrollbars().scroll({ y : targetTop}, 300);
                    }
                }
            }
        });
    }

    //The checker
    const gambitGalleryIsInView = el => {
        const scroll = window.scrollY || window.pageYOffset;
        const boundsTop = el.getBoundingClientRect().top + scroll;

        const viewport = {
            top: scroll,
            bottom: scroll + window.innerHeight,
        };

        const bounds = {
            top: boundsTop,
            bottom: boundsTop + el.clientHeight,
        };

        return ( bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom )
            || ( bounds.top <= viewport.bottom && bounds.top >= viewport.top );
    };

    //Slider
    let $slick_slider = $(selectorSlider);
    let settings_slider = {
        autoplay: false,
        arrows: false,
        accessibility: true,
        mobileFirst: true,
        touchMove: true,
        dots: false,
        speed: 300,
        slidesToShow: 1.10,
        slidesToScroll: 1,
        infinite: false,
        focusOnSelect: true
    };

    $(document).ready(function(){
        slick_on_mobile($slick_slider, settings_slider);

        //Trigger window resize
        if (typeof(Event) === 'function') {
            // modern browsers
            window.dispatchEvent(new Event('resize'));
        } else {
            // for IE and other old browsers
            // causes deprecation warning on modern browsers
            let evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
        }

        //On slider swipe
        $(selectorSlider).on('swipe', function(event, slick, direction){

            //Click on slider
            $('.slick-active a.c-return-point-card--link').trigger('click');
        });
    });

    //Slick on mobile
    function slick_on_mobile(slider, settings) {
        $(window).on('load resize', function () {

            //Initialize scroll bar
            initializeScrollBar();

            //If wide screen
            if ($(window).outerWidth() > 767) {

                //If slick has initialized
                if (slider.hasClass('slick-initialized')) {

                    //Destroy slick
                    slider.slick('unslick');
                }
                return;
            }

            //If slick not initialized
            if (!slider.hasClass('slick-initialized')) {

                //Initiate slick
                slider.slick(settings);
            }
        });
    }

});

export default createMapView;