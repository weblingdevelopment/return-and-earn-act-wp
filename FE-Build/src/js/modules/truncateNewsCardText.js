import truncateText from './truncateText';

var truncateNewsCardText = (function () {

    var setUpTruncateNewsCardText = function () {
      $('.c-news-card__item').each(function(){
        var longTitle = $.trim($(this).find('h4').text());
        var longSummary = $.trim($(this).find('.c-news-card-copy__summary p').text());
        var truncateTitle ='';
        var truncateSummary = '';
        // truncate title
        if(longTitle.length > 55){
          truncateTitle = truncateText(longTitle, 54);
          $(this).find('h4').text(truncateTitle);
        }
        // truncate summary
        if(longSummary.length > 115){
          truncateSummary = truncateText(longSummary, 114);
          $(this).find('.c-news-card-copy__summary p').text(truncateSummary);
        }
      });
    };
  
    var init = function () {
      setUpTruncateNewsCardText();
    };
  
    return {
      init: init
    };
  })();
  
export default truncateNewsCardText;