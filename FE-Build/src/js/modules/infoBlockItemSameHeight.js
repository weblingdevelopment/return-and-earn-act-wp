import sameHeight from './sameHeight';

var infoBlockItemSameHeight = (function () {

    var setUpInfoBlockItemSameHeight = function () {

      // call same height height on page load
      makeInfoBlockItemSameHeight();

      // make same height when resize screen
      $( window ).resize(function() {
        makeInfoBlockItemSameHeight();
      });
      /**
       * same height function - only apply on desktop
       */ 
      function makeInfoBlockItemSameHeight(){
        if( $(window).outerWidth() > 767 ){
            $('.c-info-block__copy p').sameHeight(); 
        }else{
            $('.c-info-block__copy p').removeAttr('style');
        }
      }
    };

    var init = function () {
        setUpInfoBlockItemSameHeight();
    };
  
    return {
      init: init
    };
  })();
  
export default infoBlockItemSameHeight;