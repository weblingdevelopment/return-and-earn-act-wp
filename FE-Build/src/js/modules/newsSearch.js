import createNewsListView from './createNewsListView';

var newsSearch = (function () {

    var setUpNewSearch = function () {

        var allNewsData = globalNewsData;
        // console.log(allNewsData);
  
        // initialize filter check state when page go back
        initializeCheckbox();

        // create new results list
        createNewsListView(allNewsData);

        // Filters
        var $filterCheckboxes = $('.c-news-results-filters .c-search-results-filters__items input[type="checkbox"]');

        /**
         * With every filter is selected, recreate news list
         */ 
        $filterCheckboxes.on('change', function() {
            var selectedFilters = [];
            var selectedNewsItems = [];
            // All Options
            if(this.value == 'All'){
                $(this).parent().siblings().find('input').prop('checked',false);
            }else{
                /**
                 * if other filter options are selected, remove selected state for 'All'
                 */ 
                $(this).parents('ul').children().first().find('input').prop('checked',false);
            }
            // check each selected filters
            $filterCheckboxes.filter(':checked').each(function() {
                if(this.value == 'All'){
                    selectedFilters = [];
                }else{
                    selectedFilters.push((this.value).toLowerCase());
                }
            });
            // if filters array is not empty
            if(selectedFilters.length){
                for(var i=0; i<allNewsData.length; i++){
                    if(selectedFilters.indexOf((allNewsData[i].exc_news_type).toLowerCase())>-1){
                        selectedNewsItems.push(allNewsData[i]);
                    }
                }
                createNewsListView(selectedNewsItems);
            }else{
                createNewsListView(allNewsData);
            }
        });
        // remove check state when go back new list page
        function initializeCheckbox(){
            $('.c-news-results-filters .c-search-results-filters__items input[type="checkbox"]').each(function() {
                $(this).prop('checked',false);
            });
        }
    };
  
    var init = function () {
        setUpNewSearch();
    };
  
    return {
      init: init
    };
})();
  
export default newsSearch;