import sameHeight from './sameHeight';
import TruncateNewsCardText from './truncateNewsCardText';

/**
 * @param newsData
 * This function create news list view
 */

var createNewsListView = (function (newsData) {
    // init same height function
    sameHeight.init();
    
    // empty view every time before create new view
    $('.c-news-list__items').html('');
    // get current url origin
    var currentURL = window.location.origin;

    // create news list
    for(var i =0; i< newsData.length; i++){
        var newsTitle = newsData[i].exc_news_title;
        var newsUrl = newsData[i].link;
        var newsType = newsData[i].exc_news_type;
        var newsSocialType = newsData[i].exc_social_type;
        var newsImage = newsData[i].exc_news_primary_image;
        var newsSummary = newsData[i].exc_brief_summary;
        var newsPublish = newsData[i].exc_news_publish_date;
        var newsPublishDate = newsData[i].exc_news_modified_date;

        $('.c-news-list__items').append(
            "<div class='o-layout__item c-news-list__item'>\n\
                <div class='c-news-card__item'>\n\
                    <a href='" + newsUrl + "' class='c-news-card--link' " + (newsSocialType != false ? 'target="_blank"' : '') + ">\n\
                        <div class='c-news-card__image' style='background-image:url("+ newsImage +")' aria-label='News Images'></div>\n\
                        <div class='c-news-card-content'>\n\
                            <div class='c-news__info'>\n\
                                <div class='c-news__type u-1/2@xs'>\n\
                                    <span>"+ newsType +"</span>\n\
                                </div>\n\
                                <div class='c-news__publish-date u-1/2@xs'>\n\
                                    <span>"+ (newsPublish == 1 ? newsPublishDate : '') +"</span>\n\
                                </div>\n\
                            </div>\n\
                            <div class='c-news-card-copy'>\n\
                                <h4 class='c-news-card-copy__title'> " + newsTitle + "</h4>\n\
                                <div class='c-news-card-copy__summary'>\n\
                                    <p> " + newsSummary + "</p>\n\
                                </div>\n\
                                <div class='c-news-card-copy__read-more'>\n\
                                    Read more\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </a>\n\
                </div>\n\
            </div>"
        )
    }
    // truncate title and summary after list is created
    TruncateNewsCardText.init();

    // apply same height function after list is created
    $('.c-news-card-copy__summary p').sameHeight(); 
    $('.c-news-card-copy h4').sameHeight(); 
  });
  
  export default createNewsListView;