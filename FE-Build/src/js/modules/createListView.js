/**
 * @param data
 * This function create list view
 */
var createListView = (function (data) {
    //Default variables
    var returnPointData = data;
    var currentURL = window.location.origin;
    var rpIconURL = '';
    var rpTypeClass = '';

    //Iterate all return points
    for(var i =0; i< returnPointData.length; i++){
        var rpTitle = returnPointData[i].title;
        var rpType = returnPointData[i].return_point_type;
        var rpAddress = returnPointData[i].address;
        var rpUrl = returnPointData[i].return_point_url;

        //If return point is express
        if(returnPointData[i].return_point_type.toLocaleLowerCase().indexOf('express') > -1){
            rpIconURL = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/icon-express-colored.png';
            rpTypeClass = 'express';
        } else {
            rpIconURL = currentURL + '/wp-content/themes/webling-act/static/images/card-icons/icon-bd-colored.png';
            rpTypeClass = '';
        }

        //Append card in HTML
        $('.c-return-point-card__items').append(
            "<div class='c-return-point-card__item'>\n\
                <a class='c-return-point-card--link' data-id='"+ returnPointData[i].id +"'>\n\
                    <div class='c-return-point-card--type__image " + rpTypeClass + "'>\n\
                        <img src='"+ rpIconURL + "' alt='" + rpType + "'>\n\
                    </div>\n\
                    <div class='c-return-point-card__copy " + rpTypeClass + "'>\n\
                        " + rpType + "\n\
                        <div class='c-return-point-card--name__suburb'>\n\
                            <h4>" + rpTitle + "</h4>\n\
                        </div>\n\
                        <div class='c-return-point-card--address'>" + rpAddress + "</div>\n\
                    </div>\n\
                </a>\n\
                <div class='c-return-point-card__read-more'><a href='" + rpUrl + "'>more</a></div>\n\
            </div>"
        )
    }
  });
  
  export default createListView;