import truncateText from './truncateText';

var truncateInfoBlockText = (function () {

    var setUpTruncateInfoBlockText = function () {
      if( $('.c-info-block__items').length ){
          $('.c-info-block__item').each(function(){
            var longSummary = $.trim($(this).find('.c-info-block__copy p').text());
            var truncateSummary = '';
            if(longSummary.length > 190){
              truncateSummary = truncateText(longSummary, 189);
              $(this).find('.c-info-block__copy p').text(truncateSummary);
            }
          });
      }

    };
  
    var init = function () {
      setUpTruncateInfoBlockText();
    };
  
    return {
      init: init
    };
  })();
  
export default truncateInfoBlockText;