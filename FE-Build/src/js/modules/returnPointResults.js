import createListView from './createListView';
import createMapView from './createMapView';
// import globalReturnPointsData from './../../data.json';

var returnPointResults = (function () {

    //Create list and map
    var setUpreturnPointResults = function () {
      var data = globalReturnPointsData;
      createListView(data);
      createMapView(data);
    };
  
    var init = function () {
      setUpreturnPointResults();
    };
  
    return {
      init: init
    };
  })();
  
  export default returnPointResults;
  