var truncateText = function (longText, limitedNum) {
    return $.trim(longText).substring(0, limitedNum).split(" ").join(" ") + "...";
};
export default truncateText;