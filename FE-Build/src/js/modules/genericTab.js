var genericTab = (function () {

    var setUpGenericTab = function () {
        // Add sticky header on scroll
        $('.c-tabs-menu li:first').addClass('active');
        $(".c-tabs-menu a").click(function(event) {
            event.preventDefault();
            $(this).parent().addClass("active");
            $(this).parent().siblings().removeClass("active");
            var tab = $(this).attr("href");
            $(".c-tab-content").not(tab).css("display", "none");
            $(tab).fadeIn(300);
        });
    };
  
    var init = function () {
      setUpGenericTab();
    };
  
    return {
      init: init
    };
  })();
  
  export default genericTab;