import A11yDialog from 'a11y-dialog';

var popUp = (function () {
  var init = function (elPopUp, onShow) {
    // Get the dialog element (with the accessor method you want)
    var el = document.getElementById(elPopUp);
    // Instantiate a new A11yDialog module
    var dialog = new A11yDialog(el);

    dialog.on('show', function (dialogEl, triggerEl) {
      // Do something when dialog gets shown
      //$('body,html').addClass('no-scroll');
      /*$('html, body').animate(
        {
          scrollTop: $('.js-form-wrapper').offset().top - 60
        },
        0
      );

      $(dialogEl).addClass('is-active');
      $('.c-dialog__close-btn,.c-menu-icon').addClass('is-active');
      */

      $(dialogEl).addClass('is-open');
      if($('body').innerWidth() <= 1000){
        $('html,body').addClass('js-dailog-open');
      }
    });

    dialog.on('hide', function (dialogEl, triggerEl) {
      // Do something when dialog gets hidden
      //$('body,html').removeClass('no-scroll');
      /*$('html, body').animate(
        {
          scrollTop: window.currentScrollPostion
        },
        0
      );

      $('.c-dialog__close-btn,.c-menu-icon').removeClass('is-active');
      */
      $(dialogEl).removeClass('is-open');
      $('html,body').removeClass('js-dailog-open');
    });

    return dialog;
    //dialog.show()
  };

  return {
    init: init
  };
})();

export default popUp;
