var newsCardCarousel = (function () {

    var setUpcarousel = function () {
        $('.c-news-carousel__items').slick({
          autoplay: false,
          accessibility: true,
          mobileFirst: true,
          touchMove:true,
          dots: false,
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          speed: 300,
          responsive: [
              {
                breakpoint: 1024,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1
                }
              }, 
              {
                breakpoint: 680,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 1
                }
              },
              {
                breakpoint: 319,
                settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1
                }
              }
            ]
        });
    };
  
    var init = function () {
      setUpcarousel();
    };
  
    return {
      init: init
    };
})();
  
export default newsCardCarousel;
  