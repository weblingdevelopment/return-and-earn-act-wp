import sameHeight from './sameHeight';

var makeItemSameHeight = (function () {

    var setUpMakeItemSameHeight = function () {

      // call same height function on page load
      makeFormFieldsEqualHeight()

      // call same height function when resize screen
      $( window ).resize(function() {
        makeFormFieldsEqualHeight();
      });
      /**
       * same height function - only apply on desktop
       */ 
      function makeFormFieldsEqualHeight(){
        if( $(window).outerWidth() > 767 ){
          var itemQty = $("[class*='c-equal-height__item']").length;
          if(itemQty >= 2){
            for(var i=0; i<itemQty/2; i++){
              $("[class*='c-equal-height__item" + i + "']").sameHeight();
            }
          }
        }else{
          $("[class*='c-equal-height__item']").removeAttr('style');
        }
      }
    };
  
    var init = function () {
      setUpMakeItemSameHeight();
    };
  
    return {
      init: init
    };
  })();
  
export default makeItemSameHeight;