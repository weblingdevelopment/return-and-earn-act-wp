import Nav from './modules/mainNav';
import GenericTab from './modules/genericTab';
import TruncateInfoBlockText from './modules/truncateInfoBlockText';
import sameHeight from './modules/sameHeight';
import makeItemSameHeight from './modules/makeItemSameHeight';
import infoBlockItemSameHeight from './modules/infoBlockItemSameHeight';
import NewsCardCarousel from './modules/newsCardCarousel';
import NewsSearch from './modules/newsSearch';
import TruncateNewsCardText from './modules/truncateNewsCardText';
//import TruncateText from './modules/truncateText';
import ReturnPointResults from './modules/returnPointResults';

/**
 * https://github.com/jonathantneal/keyboard-focus
 * To distinguish keyboard focus and mouse focus, quite useful for accessibility
 */
import keyboardFocus from 'keyboard-focus';

$(document).ready(function(){

  Nav.init();
  GenericTab.init();
  sameHeight.init();
  makeItemSameHeight.init();
  infoBlockItemSameHeight.init();
  TruncateInfoBlockText.init();
  NewsCardCarousel.init();
  //TruncateText.init();

  if($('.c-news-card__item').length){
    TruncateNewsCardText.init();
  }
  // news result list page
  if($('.c-news-list').length){
    NewsSearch.init();
  }
  
  if($('.c-return-point-result--list-view').length > 0){
    ReturnPointResults.init();
  }
  // Load keyboard focus
  keyboardFocus(document);
  
  // for touch device
  var touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)
  if (!touchsupport){ // browser doesn't support touch
      document.documentElement.className += " non-touch"
  }

  // apply accordion
  $('.js-accordion').accordion({ buttonsGeneratedContent: 'html' });


  //Click event to scroll to top
  $('.c-scroll-to-top').click(function () {
     $('html, body').animate({scrollTop: 0}, 500);
     return false;
  });

  /**
   * Eligibility Tool carousel
   */ 
  $('.container-slick-carousel__container').on('init', function(event, slick){
      $('.c-container-returned-carousel').addClass('initialized')
  });
  $('.container-slick-carousel__container').slick({
      autoplay: false,
      accessibility: true,
      prevArrow: $('.c-container-returned-carousel').find('.c-button--left-arrow'),
      nextArrow: $('.c-container-returned-carousel').find('.c-button--right-arrow'),
      mobileFirst: true,
      touchMove:true,
      dots: true,
      dotsClass: 'o-list-inline u-margin-small carousel-dots',
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 300,
      appendDots: $('.c-container-returned-carousel__dots')
  });

  // the sameHeight functions makes all the selected elements of the same height
  $('.c-news-card-copy__summary p').sameHeight(); 
  $('.c-news-card-copy h4').sameHeight(); 
  
  if (document.location.hash) {
    $('html, body').animate({ scrollTop: $(document.location.hash).offset().top - 200}, "slow");
  }

});