<?php
if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

add_filter('timber/loader/loader', function($loader){
	$loader->addPath(__DIR__ . "/templates", "root");
	$loader->addPath(ABSPATH, "sitebasepath");
	return $loader;
});

Timber::$dirname = array('templates', 'views');

require_once(__DIR__ . '/inc/wysiwyg.php'); 	// EVERYTHING TO DO WITH WYSIWYG EDITORS
require_once(__DIR__ . '/inc/custom_twig.php');	// EVERYTHING TO DO WITH CUSTOM TWIG FILTERS
require_once(__DIR__ . '/inc/default_twig.php');	// DEFAULT TWIG FUNCTIONALITIES COPIED HERE
require_once(__DIR__ . '/inc/custom_components.php');	// CUSTOM COMPONENTS CLASS TO DEFINE ALL THE COMPONENTS RELATED STUFF FOR THE SITE
require_once(__DIR__ . '/inc/custom_form.php');	// CUSTOM VALIDATION DEFINED FOR ALL THE GRAVITY FORMS

/**
 * Include class files
 * @since 4.9.6
 */
// Theme helper and configuration files.
include(__DIR__ . '/inc/theme/ThemeConfigurationsClass.php');
include(__DIR__ . '/inc/theme/ThemeHelpersClass.php');

// Theme hooks.
include(__DIR__ . '/inc/theme/ThemeHooksClass.php');

// Acf toolkit file
include(__DIR__ . '/inc/acf/AcfToolkitClass.php');

/**
 * Register theme hooks.
 *
 */
ThemeHooksClass::registerUserSessionHook();
ThemeHooksClass::registerCategoryDefinerDefaultValueHooks();
ThemeHooksClass::registerMimeTypesHook();
ThemeHooksClass::registerUserAccessHook();
ThemeHooksClass::registerToolkitFormShortCodeHook();
ThemeHooksClass::registerFooterJavaScriptHook();
ThemeHooksClass::registerAfterToolkitFormSubmitHook();
ThemeHooksClass::registerToolkitConfirmationMessageAnchorHook();
ThemeHooksClass::registerToolkitDataControllerHook();
ThemeHooksClass::registerJsonApiControllerPathHook();

