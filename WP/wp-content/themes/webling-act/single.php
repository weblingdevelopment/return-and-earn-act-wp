<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
get_header();
$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

if ( $post->type == 'return_points') {

    $id_return_point = $post->ID;
	$context['cp_collection_point_type'] = get_field('cp_collection_point_type');
	$context['cp_title'] = get_field('cp_title');
	$context['cp_city'] = get_field('cp_city');
	$context['cp_address'] = get_field('cp_address');
	$context['cp_state'] = get_field('cp_state');
	$context['cp_postcode'] = get_field('cp_postcode');
	$context['cp_latitude'] = get_field('cp_latitude');
	$context['cp_longitude'] = get_field('cp_longitude');
	$arr_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	$time_result = array();
	for ($i=0;$i<count($arr_days); $i++) {
		$timing_values_for_day = '';
		$timing_values_for_day2 = '';
		$open_field	= strtolower($arr_days[$i]).'_open_time_1';
		$close_field = strtolower($arr_days[$i]).'_close_time_1';
		$notes = strtolower($arr_days[$i]).'_notes_1';
		
		if(get_field($open_field)) {
			$timing_values_for_day = date("g:i a",strtotime(get_field($open_field))).' - '.date("g:i a",strtotime(get_field($close_field)));
		}
		if(get_field($notes)) {
			if($timing_values_for_day!='') {
				$timing_values_for_day .= ", ";
			}
			$timing_values_for_day .= get_field($notes);
		}

		$open_field2	= strtolower($arr_days[$i]).'_open_time_2';
		$close_field2 = strtolower($arr_days[$i]).'_close_time_2';
		$notes2 = strtolower($arr_days[$i]).'_notes_2';
		
		if(get_field($open_field2)) {
			$timing_values_for_day2 = date("g:i a",strtotime(get_field($open_field2))).' - '.date("g:i a",strtotime(get_field($close_field2)));
		}
		if(get_field($notes2)) {
			if($timing_values_for_day2!='') {
				$timing_values_for_day2 .= ", ";
			}
			$timing_values_for_day2 .= get_field($notes2);
		}

		if( $timing_values_for_day != '' ) {
			$time_result[$i]['day'] = $arr_days[$i];
			if ( $timing_values_for_day2 != '' ) {
				$time_result[$i]['timings'] = $timing_values_for_day.", ".$timing_values_for_day2;
			} else {
				$time_result[$i]['timings'] = $timing_values_for_day;
			}
		}
	}
	$context['time_table'] = $time_result;

	if((get_field('cp_public_holidays')!=NULL) || (get_field('cp_public_holidays')!='')) {
		$context['ph_notes'] = get_field('cp_public_holidays');
	}

	if((get_field('cp_critical_info_content')!=NULL) || (get_field('cp_critical_info_content')!='')) {
		$context['infonotes'] = get_field('cp_critical_info_content');
	}

	// Get category definers array
    $context['category_definers'] = ThemeHelpersClass::getCategoryDefiners();

	Timber::render( 'pages/return-point-detail.twig', $context);
} elseif ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}

get_footer();