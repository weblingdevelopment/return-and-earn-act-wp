<?php /* Template Name: Master Template */ 

get_header();
$context = Timber::get_context();
$context['posts'] = new Timber\Post();

if( get_field('banner_title') ) {
    $context['header_image'] = get_field('banner_image');
    $context['header_title'] = get_field('banner_title');
    $context['header_subtitle'] = get_field('banner_sub_title');
}

if( have_rows('layouts') ) {
    $sorting_layout = 0;
    while( have_rows('layouts') ):
        the_row();
        if( get_row_layout()=='info_block' ) { // GRID COLUMN LAYOUTS, DISPLAYED AS TWO COLUMN LAYOUT
            $i = 0;
            $arr_info_block = array();
            while(have_rows('info_block_repeater')) {
                the_row();
                $arr_info_block[$i]['title'] = get_sub_field('grid_title');
                $arr_info_block[$i]['content'] = get_sub_field('grid_content');
                $arr_info_block[$i]['link'] = get_sub_field('grid_link');
                $arr_info_block[$i]['link_text'] = get_sub_field('grid_link_text');
                $i++;
            }
            $context['layout'][$sorting_layout]['info_block_key'] = $arr_info_block; //['info_block']
        } elseif ( get_row_layout()=='content_block' ) { // DISPLAYS THE CONTENTS IN THE WYSIWYG EDITOR
            if( get_sub_field('basic_content_block') ) {
                $context['layout'][$sorting_layout]['basic_content_block_key'] = get_sub_field('basic_content_block'); //['content_block']
            }
        } elseif ( get_row_layout()=='eligibility_carousel' ) { // DISPLAYS STATIC CONTENT OF THE CAROUSEL
            $context['layout'][$sorting_layout]['eligibility_carousel_key'] = true;
        } elseif ( get_row_layout()=='news_grid' ) { // DISPLAYS ALL THE ACTIVE NEWS AS A LIST

                $array_news = array();
                // USE THIS META QUERY TO FILTER 
                $args = array(
                    'post_type' => 'act_news',
                    'order'   => 'DESC'
                );

                $all_news = Timber::get_posts( $args );

                $news_types = get_field_object('field_5bbe8ea3f76b8');
                $news_type_options = $news_types['choices'];
                
                $news_array_data = array();
                $i = 0;
                foreach($all_news as $news_item) {
                    $news_array_data[$i]['exc_news_type'] =  $news_item->exc_news_type;
                    $news_array_data[$i]['link'] =  $news_item->link;
                    $news_array_data[$i]['exc_social_type'] =  $news_item->exc_social_type;
                    $news_array_data[$i]['exc_news_primary_image'] =  wp_get_attachment_url($news_item->exc_news_primary_image);
                    $news_array_data[$i]['exc_news_title'] =  $news_item->exc_news_title;
                    $news_array_data[$i]['exc_brief_summary'] =  $news_item->exc_brief_summary;
                    $news_array_data[$i]['exc_news_featured'] =  $news_item->exc_news_featured;
                    $news_array_data[$i]['exc_news_publish_date'] =  $news_item->exc_news_publish_date;
                    $news_array_data[$i]['exc_news_date_display'] =  $news_item->exc_news_date_display;
                    $news_array_data[$i]['exc_news_suburb'] =  $news_item->exc_news_suburb;
                    $news_array_data[$i]['exc_news_latitude'] =  $news_item->exc_news_latitude;
                    $news_array_data[$i]['exc_news_longitude'] =  $news_item->exc_news_longitude;
                    if ($news_item->exc_news_date_display) {
                        $news_array_data[$i]['exc_news_modified_date'] = date( 'j M Y' , strtotime($news_item->exc_news_date_display) );
                    } else {
                        $news_array_data[$i]['exc_news_modified_date'] = get_the_modified_date( 'j M Y' , $news_item->ID );
                    }                    
                    $i++;
                }

                $array_news['list_categories'] = $news_type_options;
                $array_news['allnews'] = json_encode($news_array_data); // PROVIDE JSON DATA FOR NEWS FOR FILTER USE







            $context['layout'][$sorting_layout]['news_list'] = $array_news;
        } elseif ( get_row_layout()=='accordions' ) { // DISPLAYS ANY TYPE OF ACCORDIONS            
            $arr_accordion = array();
            $arr_accordion['header_type'] = get_sub_field('accordion_header_type');
            $arr_accordion['header'] = get_sub_field('accordion_header');
            $has_accordions = get_sub_field('accordions_block');
            if ($has_accordions != null) {                
                foreach ($has_accordions as $key => $value) {
                    $id_accordion = $value->ID;
                    if (have_rows('accordions', $id_accordion)){
                        while (have_rows('accordions', $id_accordion)) {
                            the_row(); 
                            //$arr_accordion[$id_accordion]['accordion_header'] = get_field('accordions_title', $id_accordion); // ADD THE ACCORDION TITLE AS ACCORDION HEADER
                            $title_accordion = get_sub_field('acc_title', $id_accordion);
                            $content_accordion = get_sub_field('acc_content', $id_accordion);                            
                            $arr_accordion[$id_accordion][$title_accordion] = $content_accordion;
                        }
                    }
                }
            }
            $context['layout'][$sorting_layout]['accordions_key'] = $arr_accordion;
        } elseif( get_row_layout()=='two_column_with_image_block' ) { // TWO COLUMN WITH IMAGE AND CONTENT LAYOUTS
            $i = 0;
            $arr_two_column_content = array();
            while(have_rows('two_column_repeater')) {
                the_row();
                $arr_two_column_content[$i]['left_image'] = get_sub_field('left_image');
                $arr_two_column_content[$i]['right_content'] = get_sub_field('right_content');
                $i++;
            }
            $context['layout'][$sorting_layout]['two_column_with_image_block_key'] = $arr_two_column_content;
        } elseif( get_row_layout()=='component_separator_block' ) { // COMPONENT SEPARATOR BLOCK
            $context['layout'][$sorting_layout]['component_separator_block_key'] = true;
        } elseif ( get_row_layout()=='select_pre_built_content_blocks' ) { // DISPLAYS THE CONTENTS OF STATIC BLOCKS
            $content_static_block = array();
            if (get_sub_field('pre_built_content_block')) { // CHECK IF STATIC BLOCK IS DEFINED
                $static_block = get_sub_field('pre_built_content_block');
                foreach ($static_block as $key => $value) {
                    $id_static_block = $value->ID;
                    if (get_field('static_block_content', $id_static_block)){ // GET THE CONTENTS OF THE PARTICULAR STATIC BLOCK SELECTED
                        $content_static_block[$id_static_block] = get_field('static_block_content', $id_static_block);
                    }
                }
                $context['layout'][$sorting_layout]['select_pre_built_content_blocks_key'] = $content_static_block; 
            }            
        } elseif ( get_row_layout()=='news_carousel' ) { // DISPLAYS NEWS CAROUSEL

            $array_news_carousel_items = array();
            // USE THIS META QUERY TO FILTER 
            $args = array(
                'post_type' => 'act_news',
                'order'   => 'DESC',
                'meta_query' => array(
                    array(
                          'key'   => 'exc_news_featured',
                          'compare' => '=',
                          'value'   => true,
                      ),
                  ),
            );

            $all_news = Timber::get_posts( $args );

            $news_types = get_field_object('field_5bbe8ea3f76b8');
            $news_type_options = $news_types['choices'];
            
            $news_array_data = array();
            $i = 0;
            foreach($all_news as $news_item) {
                $news_array_data[$i]['exc_news_type'] =  $news_item->exc_news_type;
                $news_array_data[$i]['link'] =  $news_item->link;
                $news_array_data[$i]['exc_social_type'] =  $news_item->exc_social_type;
                $news_array_data[$i]['exc_news_primary_image'] =  wp_get_attachment_url($news_item->exc_news_primary_image);
                $news_array_data[$i]['exc_news_title'] =  $news_item->exc_news_title;
                $news_array_data[$i]['exc_brief_summary'] =  $news_item->exc_brief_summary;
                $news_array_data[$i]['exc_news_featured'] =  $news_item->exc_news_featured;
                $news_array_data[$i]['exc_news_publish_date'] =  $news_item->exc_news_publish_date;
                $news_array_data[$i]['exc_news_date_display'] =  $news_item->exc_news_date_display;
                $news_array_data[$i]['exc_news_suburb'] =  $news_item->exc_news_suburb;
                $news_array_data[$i]['exc_news_latitude'] =  $news_item->exc_news_latitude;
                $news_array_data[$i]['exc_news_longitude'] =  $news_item->exc_news_longitude;
                if ($news_item->exc_news_date_display) {
                    $news_array_data[$i]['exc_news_modified_date'] = date( 'j M Y' , strtotime($news_item->exc_news_date_display) );
                } else {
                    $news_array_data[$i]['exc_news_modified_date'] = get_the_modified_date( 'j M Y' , $news_item->ID );
                }                
                $i++;
            }

            $array_news_carousel_items['list_categories'] = $news_type_options;
            $array_news_carousel_items['allnews'] = $news_array_data; // PROVIDE JSON DATA FOR NEWS FOR FILTER USE
            
            $context['layout'][$sorting_layout]['news_carousel_key'] = $array_news_carousel_items;
        }
        $sorting_layout++;
    endwhile;    
}

Timber::render( 'pages/master.twig', $context );
get_footer();