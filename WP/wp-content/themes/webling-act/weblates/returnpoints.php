<?php /* Template Name: Return Points Template */ 

get_header();
$context = Timber::get_context();

$args = array(
    'post_type' => 'return_points',
    'post_status' => 'publish',
    'orderby'   => 'post_date',
    'order' => 'ASC'
);

$list_return_points = Timber::get_posts( $args );

$arr_return_points = array();
$count = 0;
foreach($list_return_points as $item) {

    // Finalize RP url.
    $rp_url_parts = explode('?', $item->guid);
    $rp_url = (count($rp_url_parts) == 2)
        ? implode('?', [
            site_url(),
            $rp_url_parts[1]
        ])
        : $item->guid;

    // Prepare row.
    $id_return_point = $item->ID;
    $arr_return_points[$count]['id'] = $id_return_point;
    $arr_return_points[$count]['return_point_type'] = $item->cp_collection_point_type;
    $arr_return_points[$count]['title'] = $item->cp_title;
    $arr_return_points[$count]['address'] = $item->cp_address;
    $arr_return_points[$count]['post_modified'] = $item->post_modified;
    $arr_return_points[$count]['return_point_url'] = $rp_url;
    $arr_return_points[$count]['city'] = $item->cp_city;
    $arr_return_points[$count]['state'] = $item->cp_state;
    $arr_return_points[$count]['postcode'] = $item->cp_postcode;
    $arr_return_points[$count]['latitude'] = $item->cp_latitude;
    $arr_return_points[$count]['longitude'] = $item->cp_longitude;
    $arr_return_points[$count]['status'] = $item->status;

    $count++;
}
$context['return_points'] = $arr_return_points;
Timber::render( 'pages/return-point-results.twig', $context );
get_footer();