<?php
/**
 * The Template for displaying all single news posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

get_header();
$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$context['news_detail'] = $post;
$context['news_body'] = get_field('exc_news_body');
$context['news_image'] = get_field('exc_news_primary_image');

if ($post->exc_news_date_display) {
	$context['news_published_date'] = date( 'j M Y' , strtotime( $post->exc_news_date_display ) );
} else {
	$context['news_published_date'] = get_the_modified_date( 'j M Y' , $post->ID );
}     

$has_news_links = get_field('exc_news_links_repeater');
if ($has_news_links != null) {
	$i=0;
	foreach ($has_news_links as $key => $value) {
		$context['news_links'][$i]['title'] = $value['exc_news_link_title'];
		$context['news_links'][$i]['link'] = $value['exc_news_link_url'];
		$i++;
	}
}

$has_news_attachments = get_field('exc_news_attachments_repeater');
if ($has_news_attachments != null) {
	$i=0;
	foreach ($has_news_attachments as $key => $value) {
		$context['news_attachments'][$i]['title'] = $value['exc_news_attachment_title'];
		$context['news_attachments'][$i]['link'] = $value['exc_news_attachment'];
		$context['news_attachments'][$i]['size'] = $value['exc_news_attachment_type_and_size'];
		$i++;
	}
}

Timber::render( 'pages/news-detail.twig', $context);





get_footer();