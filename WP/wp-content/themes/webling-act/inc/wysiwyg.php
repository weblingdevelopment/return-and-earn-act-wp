<?php
// ADD EXTERNAL TINYMCE PLUGIN FOR ANCHOR
function tinymce_add_external_plugins($plugins) {
    $plugins['anchor'] = '/wp-content/themes/webling-act/static/anchor/plugin.min.js';
    return $plugins;
}
add_filter('mce_external_plugins', 'tinymce_add_external_plugins');

// ADD LIST OF SPECIFIC ACTIONS AS PER THE THEME
function my_mce_before_init_insert_formats($init_array) {
    $style_formats = array(
        array(
            'title' => 'Link as primary button',
			'inline' => 'span',
			'selector' => 'a',
            'classes' => 'c-button c-button--primary-button'
        ),
        array(
            'title' => 'Link as secondary button',
			'inline' => 'span',
			'selector' => 'a',
            'classes' => 'c-button c-button--secondary-button'
        ),
        array(
            'title' => 'External Links',
			'selector' => 'a',
            'classes' => 'c-external-link'
		),
		array(
            'title' => 'Hyphen List',
			'inline' => 'span',
			'selector' => 'ul',
            'classes' => 'c-hyphen-list'
        ),
    );
	$init_array['style_formats'] = json_encode($style_formats);
    return $init_array;
}
add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');

// Callback function to insert 'styleselect' into the $buttons array
function tinymce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	array_push( $buttons, "anchor" ); // ADD ANCHOR TO THE TOOLBAR
	return $buttons;
}
add_filter('mce_buttons_2', 'tinymce_buttons_2');