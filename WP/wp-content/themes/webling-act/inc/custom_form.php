<?php
// ADD GRAVITY FORM SCRIPTS TO THE FOOTER
add_filter( 'gform_init_scripts_footer', '__return_true' );
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) { die();  // require for drop down field in form
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
	return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
	$content = ' }, false );';
	return $content;
}

add_filter( 'gform_phone_formats', 'au_phone_format' );
function au_phone_format( $phone_formats ) {
    $phone_formats['au'] = array(
        'label'       => '(##) #### ####',
        'mask'        => '(99) 9999 9999',
        'regex'       => '/^\D?(\d{2})\D?\D?(\d{4})\D?(\d{4})$/',
        'instruction' => '(##) #### ####',
    ); 
    return $phone_formats;
}

// CHANGE THE SUBMIT BUTTON TO INCLUDE CUSTOM CSS CLASS
// add_filter( 'gform_submit_button_4', 'form_submit_button', 10, 2 );
// function form_submit_button( $button, $form ) {
// 	$dom = new DOMDocument();
//     $dom->loadHTML( $button );
//     $input = $dom->getElementsByTagName( 'input' )->item(0);
// 	$getclass = $input->getAttribute( 'class' );
// 	//$getclass .= ' c-button c-button--primary-button c-button--submit-gravity';
// 	$input->setAttribute( 'class', $getclass );
// 	return '<span class="c-button c-button--primary-button"><span>'.$dom->saveHtml( $input ).'</span></span>';
// }

// NUMBER FIELD IS USED FOR ABN/ACN, GET RID OF THOUSAND SEPARATOR
add_filter( 'gform_include_thousands_sep_pre_format_number', '__return_false' );

// ADD FILTER TO VALIDATE THE ABN/ACN AS PER THE OPTION CHOSEN FOR ABN/ACN
add_filter( 'gform_field_validation_4_5', function ( $result, $value, $form, $field ) {
	$master = rgpost( 'input_3' );
	if($master == 'ABN') {
		if($value <= 9999999999 || $value > 99999999999) {
			$result['is_valid'] = false;
			$result['message']  = 'The ABN must be 11 numbers with no spaces.';
		}		
	} else if($master == 'ACN') {
		if($value <= 99999999 || $value > 999999999) {
			$result['is_valid'] = false;
			$result['message']  = 'The ACN must be 9 numbers with no spaces.';
		}
	}
    return $result;
}, 10, 4 );

// ADD FILTER TO VALIDATE THE ARRANGEMENT NUMBER FIELD, IF THE YES RADIO BUTTON FOR PREVIOUSLY SIGNED IS SELECTED
add_filter( 'gform_field_validation_4_28', function ( $result, $value, $form, $field ) {
	$master = rgpost( 'input_12' );
	if($master == 'Yes') {
		if($result['is_valid'] && empty($value) ) {
			$result['is_valid'] = false;
			$result['message']  = 'The arrangement number is required when already signed up as a supplier.';
		}
	}
    return $result;
}, 10, 4 );

// ADD FILTER TO VALIDATE FILE UPLOAD FIELD SIGNATORY 1, IF THE NO RADIO BUTTON FOR PREVIOUSLY SIGNED IS SELECTED
add_filter( 'gform_field_validation_4_19', function ( $result, $value, $form, $field ) {
	$master = rgpost( 'input_12' );
	if($master == 'No') {
		if(!$result['is_valid']) {
			$result['is_valid'] = false;
			$result['message']  = 'The first signatory file is required when not already signed up as a supplier.';
		}
	} else {
		$result['is_valid'] = true;
		$result['message']  = '';
	}
    return $result;
}, 10, 4 );

// ADD FILTER TO VALIDATE FILE UPLOAD FIELD SIGNATORY 2, IF THE NO RADIO BUTTON FOR PREVIOUSLY SIGNED IS SELECTED AND SIGNATORY FIELDS 2 ARE FILLED
add_filter( 'gform_field_validation_4_24', function ( $result, $value, $form, $field ) {
	$master = rgpost( 'input_12' );
	if($master == 'No') {
		if(!$result['is_valid']) {
			$result['is_valid'] = false;
			$result['message']  = 'The second signatory file is required with other second signatory details.';
		}
	} else {
		$result['is_valid'] = true;
		$result['message']  = '';
	}
    return $result;
}, 10, 4 );


// CHANGE THE UPLOAD FILE PATH
add_filter( 'gform_upload_path', 'change_upload_path', 10, 2 );
function change_upload_path( $path_info, $form_id ) {
	$path_i = explode('wp-content/uploads/gravity_forms/', $path_info['path']);
	$path_info['path'] = FILE_PATH_GF.$path_i[1]; 
	   
	$newpath = str_replace("wp-content/uploads/gravity_forms/", JPT_FILE_PATH_GF, $path_info['url']); 
	$path_info['url'] = $newpath;
   	return $path_info;
}

// BEFORE SENDING NOTIFICATION TO ADMIN EMAIL FROM EXPORTER FORM, ZIP THE FILES AND ATTACH IT TO THE EMAIL
add_filter( 'gform_notification_4', 'change_user_notification_attachments', 10, 3 );
function change_user_notification_attachments( $notification, $form, $entry ) {

    // Basics.
    $zip_archive_prefix = 'supplier-form-attachments-';

    // If this is the correct form?
	if ( $notification['name'] == 'Admin Notification' ) {
		
        $fileupload_fields = GFCommon::get_fields_by_type( $form, array( 'fileupload' ) );
		if ( ! is_array( $fileupload_fields ) )
            return $notification;
 
        $notification['attachments'] = ( is_array( rgget('attachments', $notification ) ) ) ? rgget( 'attachments', $notification ) : array();
		$upload_root = FILE_PATH_GF;

        // Finalize file name.
        if(!empty(rgar($entry, '1'))) {
            $tmp_file_name = str_replace(" ", "-", rgar($entry, '1'));
            $zipfilename = implode('', [
                FILE_PATH_GF,
                $zip_archive_prefix,
                strtolower($tmp_file_name) . '-',
                $entry['id'],
                '.zip'
            ]);

        } else {
            $zipfilename = implode('', [
                FILE_PATH_GF,
                $zip_archive_prefix,
                $entry['id'],
                '.zip'
            ]);
        }

        $zippedfile = 0;
		$zip = new ZipArchive;
		if ($zip->open($zipfilename, ZipArchive::CREATE) === TRUE) {

			foreach( $fileupload_fields as $field ) {
				$url = $entry[ $field->id ];
				if(!empty($url)) {
					$attachment = preg_replace( '|^(.*?)/files/|', $upload_root, $url );
					if($field->id ==  19) {
						$zip->addFile($attachment, 'signatory1/'.pathinfo( $attachment, PATHINFO_BASENAME ));
					} else {
						$zip->addFile($attachment, 'signatory2/'.pathinfo( $attachment, PATHINFO_BASENAME ));
					}
					$zippedfile++;
				}
			}
			$zip->close();			
			if ( $zippedfile > 0 ) {
				$notification['attachments'][] = $zipfilename;			
			}
		}
	}
	
    return $notification;
}

// DELETE THE FILES FOR THE EXPORTER FORM
// ENCRYPT THE FILE UPLOADED ONCE THE FORM IS SUBMITTED
add_action( 'gform_after_submission_4', 'post_to_third_party', 10, 2 );
function post_to_third_party( $entry, $form ) {

    // Basics.
    $zip_archive_prefix = 'supplier-form-attachments-';

	if(!empty(rgar($entry, '19'))) {
		$file_path = explode(JPT_FILE_PATH_GF, rgar($entry, '19'));
		$original_file_path = FILE_PATH_GF.$file_path[1]; 

		unlink($original_file_path);
	}

	if(!empty(rgar($entry, '24'))) {
		$file_path = explode(JPT_FILE_PATH_GF, rgar($entry, '24'));
		$original_file_path = FILE_PATH_GF.$file_path[1]; 
		
		unlink($original_file_path);
	}

    // Finalize file name.
    if(!empty(rgar($entry, '1'))) {
        $tmp_file_name = str_replace(" ", "-", rgar($entry, '1'));
        $zipfile_name = implode('', [
            FILE_PATH_GF,
            $zip_archive_prefix,
            strtolower($tmp_file_name) . '-',
            $entry['id'],
            '.zip'
        ]);

    } else {
        $zipfile_name = implode('', [
            FILE_PATH_GF,
            $zip_archive_prefix,
            $entry['id'],
            '.zip'
        ]);
    }

	// Do we have the file?
	if(file_exists($zipfile_name)) {
		$enc = EncryptData(file_get_contents($zipfile_name));
		file_put_contents($zipfile_name, $enc);
	}
}

// HIDE THE UPLOAD FILE IN THE ADMIN ENTRY SECTION
add_filter( 'gform_entry_field_value', function ( $value, $field, $entry, $form ) {
    if ( $field->get_input_type() == 'fileupload' && ! empty( $value ) ) { // Single file upload field
        $file_url = rgar( $entry, $field->id );
		$value = sprintf( "For security reasons, files are attached to email. Please check email.", $file_url);
    }  
    return $value;
}, 10, 4 );


// ENCRYPT THE DATA USING THE PUBLIC KEY, USED FOR ENCRYPTING THE ZIP FILE
function EncryptData($source) 
{ 
	$read_file = fopen(PUBLIC_KEY_PATH, "r");
	$pub_key_raw = fread($read_file, 8192);
	fclose($read_file);
	$pub_key = openssl_get_publickey(trim($pub_key_raw));

	openssl_seal($source, $sealed, $ekey, array($pub_key));
	$ekey = base64_encode($ekey[0]);

	$value = "enmirum10:".$ekey.':'.base64_encode($sealed);
	openssl_free_key($pub_key);
	return($value); 
}

/*
 * Starts: Implementation of "Exporter form" attachment in email.
 *
 *
 **/
// BEFORE SENDING NOTIFICATION TO ADMIN EMAIL FROM EXPORTER FORM, ZIP THE FILES AND ATTACH IT TO THE EMAIL
add_filter( 'gform_notification_8', 'change_user_exporter_notification_attachments', 10, 3 );
function change_user_exporter_notification_attachments( $notification, $form, $entry ) {

    // Basics.
    $zip_archive_prefix = 'exporter-form-attachments-';

    // Is this the correct notification?
    if ( $notification['name'] == 'Admin Notification' ) {

        $fileupload_fields = GFCommon::get_fields_by_type( $form, array( 'fileupload' ) );
        if ( ! is_array( $fileupload_fields ) )
            return $notification;

        $notification['attachments'] = ( is_array( rgget('attachments', $notification ) ) ) ? rgget( 'attachments', $notification ) : array();
        $upload_root = FILE_PATH_GF;

        // Finalize file name.
        if(!empty(rgar($entry, '1'))) {
            $tmp_file_name = str_replace(" ", "-", rgar($entry, '1'));
            $zipfilename = implode('', [
                FILE_PATH_GF,
                $zip_archive_prefix,
                strtolower($tmp_file_name) . '-',
                $entry['id'],
                '.zip'
            ]);

        } else {
            $zipfilename = implode('', [
                FILE_PATH_GF,
                $zip_archive_prefix,
                $entry['id'],
                '.zip'
            ]);
        }

        $zippedfile = 0;
        $zip = new ZipArchive;
        if ($zip->open($zipfilename, ZipArchive::CREATE) === TRUE) {

            foreach( $fileupload_fields as $field ) {
                $url = $entry[ $field->id ];
                if(!empty($url)) {
                    $attachment = preg_replace( '|^(.*?)/files/|', $upload_root, $url );
                    if($field->id ==  19) {
                        $zip->addFile($attachment, 'signatory1/'.pathinfo( $attachment, PATHINFO_BASENAME ));
                    } else {
                        $zip->addFile($attachment, 'signatory2/'.pathinfo( $attachment, PATHINFO_BASENAME ));
                    }
                    $zippedfile++;
                }
            }
            $zip->close();
            if ( $zippedfile > 0 ) {
                $notification['attachments'][] = $zipfilename;
            }
        }
    }

    return $notification;
}

// DELETE THE FILES FOR THE EXPORTER FORM
// ENCRYPT THE FILE UPLOADED ONCE THE FORM IS SUBMITTED
//add_action( 'gform_after_submission_8', 'post_exporter_form_to_third_party', 10, 2 );
function post_exporter_form_to_third_party( $entry, $form ) {

    // Basics.
    $zip_archive_prefix = 'exporter-form-attachments-';

    if(!empty(rgar($entry, '19'))) {
        $file_path = explode(JPT_FILE_PATH_GF, rgar($entry, '19'));
        $original_file_path = FILE_PATH_GF.$file_path[1];

        unlink($original_file_path);
    }

    if(!empty(rgar($entry, '24'))) {
        $file_path = explode(JPT_FILE_PATH_GF, rgar($entry, '24'));
        $original_file_path = FILE_PATH_GF.$file_path[1];

        unlink($original_file_path);
    }

    // Finalize file name.
    if(!empty(rgar($entry, '1'))) {
        $tmp_file_name = str_replace(" ", "-", rgar($entry, '1'));
        $zipfile_name = implode('', [
            FILE_PATH_GF,
            $zip_archive_prefix,
            strtolower($tmp_file_name) . '-',
            $entry['id'],
            '.zip'
        ]);

    } else {
        $zipfile_name = implode('', [
            FILE_PATH_GF,
            $zip_archive_prefix,
            $entry['id'],
            '.zip'
        ]);
    }

    // Do we have the file?
    if(file_exists($zipfile_name)) {
        $enc = EncryptData(file_get_contents($zipfile_name));
        file_put_contents($zipfile_name, $enc);
    }
}

/*
 * Ends: Implementation of "Exporter form" attachment in email.
 *
 *
 **/
