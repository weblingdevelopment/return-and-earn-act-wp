<?php

class AcfToolkitClass {

    /**
     * Class properties.
     *
     */
    public static $tookitDownloadWarning = '<p><i>By downloading and using this toolkit you agree to the terms of use: material must not be altered. Information may only be added into the editable fields, where provided.</i></p>';

    /**
     * Constructor.
     *
     */
    public function __construct()
    {
    }

    /**
     * Prepare toolkit html against a post.
     *
     * @param $postId
     *
     * @return string
     */
    public static function prepareToolkitHtml( $postId ) {

        // Basics.
        $response = '';
        $isGravityFormSubmitted = !empty($_POST);
        $toolkitApiUrl = get_site_url().'/api/toolkitdata/reportToolkitUsage';
        $toolkitGfConfigurations = ThemeConfigurationsClass::$themeConfigurations['gravityForm']['toolkit'];

        // Get post toolkit cookies.
        $postToolkitCookies = self::getPostToolkitCookies(
            $postId,
            $toolkitGfConfigurations
        );

        // Get prepared toolkit content.
        $toolkitPreparedContent = self::getToolkitPreparedContent(
            $postId,
            true
        );


        /**
         * Let's start running validations on cookie data.
         *
         */
        // Check if toolkit post id exists?
        if (
            $postId // we have toolkit post id.
            && !empty($toolkitPreparedContent['files']) // we have files.
        ) {

            // Basics.
            $requestStatus = [
                'hasRecordEntry' => false,
                'toolkitDownloadedEarlier' => false,
                'jsButtonSubmitEvent' => null,
            ];

            // Check if cookie has a record of this toolkit downloaded earlier?
            if (
                $postToolkitCookies['toolkitId']
                && htmlspecialchars($postToolkitCookies['toolkitId']) == $postId
            ) {
                $requestStatus['toolkitDownloadedEarlier'] = true;
            }

            // If cookie has a record of this toolkit downloaded earlier?
            // In this case we have email set in cookie but it's not in gravity forms.
            if (
                $postToolkitCookies['email']
                && $requestStatus['toolkitDownloadedEarlier']
            ) {

                // Set existing form entries of toolkit for this post.
                $postToolkitFormEntries = self::getPostToolkitFormEntries(
                    $postId,
                    $toolkitGfConfigurations,
                    $postToolkitCookies['email']
                );

                // If we have form entries?
                if (count($postToolkitFormEntries) > 0) {
                    $requestStatus['hasRecordEntry'] = true;

                } else {
                    // Set button submit event.
                    $listCookies = json_encode($_COOKIE);
                    $requestStatus['jsButtonSubmitEvent'] = implode('', [
                        "onclick='CheckPreviousFormSubmitForThisToolkitForm(",
                        $postId,
                        ", `",
                        $toolkitPreparedContent['title'],
                        "`, ",
                        $listCookies,
                        ", `",
                        $toolkitApiUrl,
                        "`);' onkeypress='CheckPreviousFormSubmitForThisToolkitForm(",
                        $postId,
                        ", `",
                        $toolkitPreparedContent['title'],
                        "`, ",
                        $listCookies,
                        ", `",
                        $toolkitApiUrl,
                        "`);'"
                    ]);

                    // Update toolkit files with this event.
                    foreach ($toolkitPreparedContent['files'] as $key => $file) {
                        $toolkitPreparedContent['files'][$key] = str_replace(
                            '{jsButtonSubmitEventVariable}',
                            $requestStatus['jsButtonSubmitEvent'],
                            $file
                        );
                    }
                }
            }

            // Is email set in cookie?
            if (
                $postToolkitCookies['email']
                && $requestStatus['toolkitDownloadedEarlier']
            ) {
                $response = implode('', [
                    '<div class="c-download-toolkits--success">',
                    implode('', [
                        '<h3>',
                        $toolkitPreparedContent['title'],
                        '</h3>',
                        ThemeHelpersClass::getSessionSuccessMessages(),
                        self::$tookitDownloadWarning,
                        implode('', $toolkitPreparedContent['files'])
                    ]),
                    '</div>'
                ]);

            } else {

                // Do gravity form shortcode.
                $thisIsGravityForm = do_shortcode(implode('', [
                    '[gravityform id="',
                    $toolkitGfConfigurations['formId'],
                    '" title="false" description="false" ajax="false" field_values="toolkit-type=',
                    $toolkitPreparedContent['title'],
                    '&toolkit-id=',
                    $postId,
                    '"]'
                ]));

                // Finalize response.
                $response = implode('', [
                    '<div class="u-margin-vertical-large">',
                    implode('', [
                        '<div class="c-download-toolkits__container" data-toolkits-id='.$postId.'>',
                        implode('', [
                            '<div class="c-download-toolkits--copy">',
                            implode('', [
                                '<h3>'.$toolkitPreparedContent['title'].'</h3>',
                                (strpos($toolkitPreparedContent['description'], '<p>') !== false
                                    ? $toolkitPreparedContent['description']
                                    : '<p class="c-download-toolkits--subtext">' . $toolkitPreparedContent['description'] . '</p>'
                                ),
                                '<button class="c-button c-button--primary-button c-show-download-toolkits-form ' . ($isGravityFormSubmitted ? 'u-hidden' : '') . '" name="PrimaryButton" data-toolkit-id="'.$postId.'"><span>Confirm your details to download</span></button>'
                            ]),
                            '</div>',
                            '<div class="c-download-toolkits--form ' . ($isGravityFormSubmitted ? '' : 'u-hidden') . '">'.
                            implode('', [
                                '<div class="u-margin-vertical-large">'.$thisIsGravityForm.'</div>'
                            ]),
                            '</div>',
                            '<div class="c-download-toolkits--success-wrapper"></div>'
                        ]),
                        '</div>'
                    ]),
                    '</div>'
                ]);
            }
        }

        // Return.
        return $response;
    }

    /**
     * Get toolkit form entries from gravity entries.
     *
     * @param $postId integer
     * @param $toolkitGfConfigurations array
     * @param $email string
     *
     * @return array
     */
    public static function getPostToolkitFormEntries($postId, $toolkitGfConfigurations, $email) {

        // Basics.
        $filteredEntries = [];

        // Finalize search criteria.
        $searchCriteria = array(
            'status'        => 'active',
            'field_filters' => array(
                'mode' => 'all',
                array(
                    'key'   => $toolkitGfConfigurations['fields']['email']['id'], // EMAIL FORM
                    'value' => $email
                ),
                array(
                    'key'   => $toolkitGfConfigurations['fields']['toolkitId']['id'], // TOOLKIT ID
                    'value' => $postId
                )
            )
        );

        // Get search results.
        $searchedEntries  = GFAPI::get_entries(
            $toolkitGfConfigurations['formId'],
            $searchCriteria
        );

        // If we have valid entries?
        if (
            is_array($searchedEntries)
            && count($searchedEntries) > 0
        ) {
            $filteredEntries = $searchedEntries;
        }

        //Return.
        return $filteredEntries;
    }

    /**
     * Return prepared toolkit content.
     *
     * @param $postId integer
     * @param $setButtonSubmitEventVariable boolean
     *
     * @return array
     */
    public static function getToolkitPreparedContent($postId, $setButtonSubmitEventVariable = false) {

        // Get toolkit fields
        $websiteToolkitGroups = get_field( 'website_toolkit_fields', $postId );

        // Finalize toolkit group.
        $selectedToolkitGroup = $websiteToolkitGroups[0];

        // Prepare toolkit content.
        $toolkitPreparedContent = [
            'title' => $selectedToolkitGroup['toolkit_title'],
            'description' => $selectedToolkitGroup['toolkit_description'],
            'files' => [],
        ];

        // Do we have toolkit files?
        for ($i=0; $i < count($selectedToolkitGroup['toolkit_files']); $i++) {

            // Basics.
            $row = $selectedToolkitGroup['toolkit_files'][$i];
            $buttonUrl = ($row['attachment_type'] == 'upload')
                ? $row['attachment_upload']
                : $row['attachment_link'];

            // Add file to array.
            $toolkitPreparedContent['files'][] = implode('', [
                '<a href="',
                $buttonUrl,
                '" ',
                $setButtonSubmitEventVariable ? '{jsButtonSubmitEventVariable}' : '', // set this variable only if $setButtonSubmitEventVariable is true?
                ' target="_blank" style="padding-right:15px;">',
                '<button class="c-button c-button--primary-button" name="PrimaryButton"><span>',
                $row['attachment_title'],
                '</span></button></a>'
            ]);
        }

        // Return.
        return $toolkitPreparedContent;
    }

    /**
     * Return cookie name.
     *
     * @param $postId integer
     * @param $gfFieldConfigurations array
     *
     * @return mixed
     */
    public static function getPostFieldToolkitCookieName($postId, $gfFieldConfigurations) {
        return implode('_', [
            $gfFieldConfigurations['cookieName'],
            $postId
        ]);
    }

    /**
     * Return cookie value for entire gravity form against a post.
     *
     * @param $postId integer
     * @param $toolkitGfConfigurations array
     *
     * @return array
     */
    public static function getPostToolkitCookies( $postId, $toolkitGfConfigurations) {

        // Basics.
        $response = [];

        // Iterate all fields.
        foreach ($toolkitGfConfigurations['fields'] as $gfFieldKey => $gfFieldConfigurations) {
            $response[$gfFieldKey] = self::getPostFieldToolkitCookie(
                $postId,
                $gfFieldConfigurations
            );
        }

        // Return.
        return $response;
    }

    /**
     * Return cookie value for a gravity form field against a post.
     *
     * @param $postId integer
     * @param $gfFieldConfigurations array
     *
     * @return mixed
     */
    public static function getPostFieldToolkitCookie( $postId, $gfFieldConfigurations) {

        // Basics.
        $response = null;

        // If we have valid $gfFieldKey?
        if (!empty($gfFieldConfigurations['cookieName'])) {

            // Set cookie field name.
            $cookieFieldName = self::getPostFieldToolkitCookieName(
                $postId,
                $gfFieldConfigurations
            );

            // Do we have this cookie set?
            if (isset($_COOKIE[$cookieFieldName])) {
                $response = $_COOKIE[$cookieFieldName];
            }
        }

        // Return.
        return $response;
    }

    /**
     * Display the confirmation after the form post.
     *
     * @param $formData array
     *
     * @return string
     */
    public static function getToolkitConfirmation($formData) {

        // Basics.
        $toolkitGfConfigurations = ThemeConfigurationsClass::$themeConfigurations['gravityForm']['toolkit'];

        // Get post id.
        $gfFormToolkitFieldId = $toolkitGfConfigurations['fields']['toolkitId']['id'];
        $postId = !empty($formData[$gfFormToolkitFieldId])
            ? $formData[$gfFormToolkitFieldId]
            : null;

        // If post id exists.
        if ($postId) {

            // Let's set cookies.
            self::setPostToolkitCookies(
                $postId,
                $toolkitGfConfigurations,
                $formData
            );
        }

        // Return.
        return 'Thank you, we have received your details. You can now access and download the toolkits.';
    }

    /**
     * Set cookies for entire gravity form fields against a post.
     *
     * @param $postId integer
     * @param $toolkitGfConfigurations array
     * @param $formData array
     *
     * @return void
     */
    public static function setPostToolkitCookies($postId, $toolkitGfConfigurations, $formData) {

        // Iterate all fields.
        foreach ($toolkitGfConfigurations['fields'] as $gfFieldKey => $gfFieldConfigurations) {

            // If we have valid $gfFieldKey?
            if (!empty($gfFieldConfigurations['cookieName'])) {

                // Basics.
                $gfFormFieldId = $gfFieldConfigurations['id'];

                // Set cookie field name.
                $cookieFieldName = self::getPostFieldToolkitCookieName(
                    $postId,
                    $gfFieldConfigurations
                );

                // If we have value of the field in $formData?
                if (isset($formData[$gfFormFieldId])) {

                    // Now set cookie.
                    setcookie($cookieFieldName, $formData[$gfFormFieldId], time() + 31536000, COOKIEPATH, COOKIE_DOMAIN, false, false);
                }
            }
        }
    }

    /**
     * Set cookies for entire gravity form fields against a post.
     *
     * @param $postId integer
     * @param $toolkitGfConfigurations array
     * @param $formData array
     *
     * @return array
     */
    public static function getPreparedPostToolkitGravityFormData($postId, $toolkitGfConfigurations, $formData) {

        // Gravity form data.
        $response = [
            'form_id' => $toolkitGfConfigurations['formId']
        ];

        // Iterate all fields.
        foreach ($toolkitGfConfigurations['fields'] as $gfFieldKey => $gfFieldConfigurations) {

            // If we have valid $gfFieldKey?
            if (!empty($gfFieldConfigurations['cookieName'])) {

                // Basics.
                $gfFormFieldId = $gfFieldConfigurations['id'];

                // Set cookie field name.
                $cookieFieldName = self::getPostFieldToolkitCookieName(
                    $postId,
                    $gfFieldConfigurations
                );

                // Now add it to response.
                $response[$gfFormFieldId] = $formData[$cookieFieldName];
            }
        }

        // Return.
        return $response;
    }
}
?>