<?php
add_filter('timber/twig', 'add_to_twig');
function add_to_twig($twig) {
    $twig->addExtension(new Twig_Extension_StringLoader());
	// $twig->addFilter(new Twig_SimpleFilter('twig_getStaticPins', 'getStaticPins'));
    return $twig;
}

// function getStaticPins($cp_type) {
// 	$pngs_arr = array(
// 		'over the counter' => '/static/images/icons/Map-Icon_OTC.png',
// 		'reverse vending machine' => '/static/images/icons/Map-Icon_RVM.png',
// 		'automated depot' => '/static/images/icons/Map-Icon_AD.png',
// 		'donation station' => '/static/images/icons/Map-Icon_DS.png'
// 	);
// 	return $pngs_arr[strtolower($cp_type)];
// }

