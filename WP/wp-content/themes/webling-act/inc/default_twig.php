<?php
class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'register_custom_menus' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function register_custom_menus() {
		register_nav_menu('nav-header', __('Navbar Header'));
		register_nav_menu('nav-footer', __('Navbar Footer'));
	}

	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu('nav-header');
		$context['footermenu'] = new TimberMenu('nav-footer');
		$context['site'] = $this;
		$context['footercontact'] = new Timber\Post('footer-contact-block'); // GET THE TOP FOOTER BLOCK TEXTS
		$context['footersocial'] = new Timber\Post('footer-social-block'); // GET THE FOOTER SOCIAL BLOCK TEXTS		
		$context['social_share_news'] = new Timber\Post('social-share-buttons-news'); // NEWS SOCIAL SHARE
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new StarterSite();