<?php
class CustomComponents {
    public function __construct() {
        add_filter('lay_the_layout', array($this, 'get_layout'), 10, 2);
    }

    public function get_layout($layout, $arg) {
        return $layout;
    }
}

new CustomComponents();