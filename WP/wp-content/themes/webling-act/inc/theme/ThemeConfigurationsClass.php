<?php

/**
 * Class to store all the project configurations.
 *
 */
class ThemeConfigurationsClass
{

    /**
     * Class properties.
     *
     */
    public static $themeConfigurations = [
        'permissions' => [
            'acf' => [
                'allowUserIds' => [ 1 ]
            ]
        ],
        'gravityForm' => [
            'toolkit' => [
                'formId' => 9,
                'fields' => [
                    'firstName' => [
                        'id' => '1',
                        'cookieName' => 'gf_tk_form_fname',
                    ],
                    'lastName' => [
                        'id' => '2',
                        'cookieName' => 'gf_tk_form_lname',
                    ],
                    'organization' => [
                        'id' => '3',
                        'cookieName' => 'gf_tk_form_company',
                    ],
                    'email' => [
                        'id' => '4',
                        'cookieName' => 'gf_tk_form_email',
                    ],
                    'consent' => [
                        'id' => '5.1',
                        'cookieName' => 'gf_tk_form_consent',
                    ],
                    'phone' => [
                        'id' => '9',
                        'cookieName' => 'gf_tk_form_phone',
                    ],
                    'downloadToolkit' => [
                        'id' => '6',
                        'cookieName' => 'gf_tk_form_toolkit_type',
                    ],
                    'toolkitId' => [
                        'id' => '7',
                        'cookieName' => 'gf_tk_form_toolkit_id',
                    ],
                ],
            ]
        ]
    ];


    /**
     * Constructor.
     *
     */
    public function __construct()
    {
    }
}