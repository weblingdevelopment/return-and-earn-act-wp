<?php
/*
Controller name: Toolkit Controller
Controller description: Toolkit APIs
*/

class JSON_API_Toolkitdata_Controller {

    /**
     * Report toolkit use.
     *
     */
    public function reportToolkitUsage() {

        // Basics.
        $response = 'Invalid form submission.';

        // Check for $_POST has data?
        if(
            isset($_POST)
            && !empty($_POST['toolkitid'])
            && !empty($_POST['toolkittype'])
            && !empty($_POST['listallcookie'])
        ) {

            // Basics.
            $toolkitGfConfigurations = ThemeConfigurationsClass::$themeConfigurations['gravityForm']['toolkit'];

            // Get data from post.
            $postId = $_POST['toolkitid'];
            $getCookieList = $_POST['listallcookie'];

            /**
             * Add some fields to $getCookieList.
             *
             */
            // Add toolkitId field data.
            $gfFormToolkitFieldKey = AcfToolkitClass::getPostFieldToolkitCookieName(
                $postId,
                $toolkitGfConfigurations['fields']['toolkitId']
            );
            $getCookieList[$gfFormToolkitFieldKey] = $_POST['toolkitid'];

            // Add toolkitType field data.
            $gfFormToolkitTypeFieldKey = AcfToolkitClass::getPostFieldToolkitCookieName(
                $postId,
                $toolkitGfConfigurations['fields']['downloadToolkit']
            );
            $getCookieList[$gfFormToolkitTypeFieldKey] = $_POST['toolkittype'];


            /**
             * Validate and add gravity form entry.
             *
             */
            // Get prepared gravity form data.
            $postToolkitGravityFormData = AcfToolkitClass::getPreparedPostToolkitGravityFormData(
                $postId,
                $toolkitGfConfigurations,
                $getCookieList
            );

            // Set email field key.
            $gfFormEmailFieldId = $toolkitGfConfigurations['fields']['email']['id'];

            // Do we have email available?
            if (!empty($postToolkitGravityFormData[$gfFormEmailFieldId])) {

                // Check if record exists in gravity form,
                $existingGravityFormEntries = AcfToolkitClass::getPostToolkitFormEntries(
                    $postId,
                    $toolkitGfConfigurations,
                    $postToolkitGravityFormData[$gfFormEmailFieldId]
                );

                // If we do not have entries?
                if (count($existingGravityFormEntries) == 0) {
                    $gravityFormEntryId = GFAPI::add_entry(
                        $postToolkitGravityFormData
                    );

                    // Update response.
                    $response = 'Form submission saved.';

                } else {
                    // Update response.
                    $response = 'Form submission not needed.';
                }
            }
        }

        // Return.
        return $response;
    }
}

?>