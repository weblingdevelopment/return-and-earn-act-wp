<?php
include( __DIR__ . '/ThemeActionsClass.php');

class ThemeHooksClass
{
    /**
     * Class properties.
     */


    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Register Category Definer ACF Relation Hooks
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerCategoryDefinerDefaultValueHooks() {
        add_filter(
            'acf/load_value/type=relationship',
            array(
                'ThemeActionsClass',
                'registerCategoryDefinerDefaultValue'
            ),
            10,
            3
        );
    }

    /**
     * Register mime types hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerMimeTypesHook() {
        add_filter(
            'upload_mimes',
            array(
                'ThemeActionsClass',
                'registerAdditionalMimeTypes'
            )
        );
    }

    /**
     * Register user access hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerUserAccessHook() {
        add_action(
            'admin_menu',
            array(
                'ThemeActionsClass',
                'registerAdminMenuUserAccess'
            )
        );
    }

    /**
     * Register toolkit form short code hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerToolkitFormShortCodeHook() {
        add_shortcode(
            'toolkit',
            array(
                'ThemeActionsClass',
                'registerToolkitFormShortCode'
            )
        );
    }

    /**
     * Register footer javascript hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerFooterJavaScriptHook() {
        add_action(
            'wp_footer',
            array(
                'ThemeActionsClass',
                'registerFooterJavaScript'
            )
        );
    }

    /**
     * Register after toolkit form submit hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerAfterToolkitFormSubmitHook() {
        add_filter(
            'gform_confirmation_'.ThemeConfigurationsClass::$themeConfigurations['gravityForm']['toolkit']['formId'],
            array(
                'ThemeActionsClass',
                'registerAfterToolkitFormSubmit'
            ),
            10,
            4
        );
    }

    /**
     *  Register return point data controller hook.
     *	Adding a API for getting Return point data
     *	This data is used by the search result page for the return point
     *	This functionality stores a json file which is used by the search result page to display return points
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerToolkitDataControllerHook() {
        add_filter(
            'json_api_controllers',
            array(
                'ThemeActionsClass',
                'registerToolkitDataController'
            )
        );
    }

    /**
     * Register jason api controller path hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerJsonApiControllerPathHook() {
        add_filter(
            'json_api_toolkitdata_controller_path',
            array(
                'ThemeActionsClass',
                'registerJsonApiControllerPath'
            )
        );
    }

    /**
     * Register toolkit confirmation message anchor hook.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerToolkitConfirmationMessageAnchorHook() {
        add_filter(
            'gform_confirmation_anchor_'.ThemeConfigurationsClass::$themeConfigurations['gravityForm']['toolkit']['formId'],
            '__return_false'
        );
    }

    /**
     * Register user session.
     *
     * @since   1.0.0
     * @return  void
     */
    public static function registerUserSessionHook() {
        add_action(
            'init',
            array(
                'ThemeActionsClass',
                'registerUserSession'
            )
        );
    }
}