<?php

class ThemeHelpersClass
{
    /**
     * Class properties.
     */
    protected static $themeMessageConfigurations = [
        'sessionName' => 'webling_session_messages',
        'types' => [
            'error' => [],
            'success' => [],
        ],
    ];


    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get category definers
     * @return array
     */
    public static function getCategoryDefiners()
    {
        // Get Category
        $selectedCategoryDefiners = get_field('components_return_points_category_definers');

        // If category definer post found
        $category_definers = [];
        if ( !empty($selectedCategoryDefiners) ) {

            foreach ($selectedCategoryDefiners as $selectedCategoryDefiner) {
                setup_postdata($selectedCategoryDefiner);

                // Prepare category definer array
                $category_definers[$selectedCategoryDefiner->ID] = self::prepareCategoryDefinerArray( $selectedCategoryDefiner->ID );
            }

            wp_reset_postdata();

        } else if( is_null($selectedCategoryDefiners) ) {

            $post_ids = get_posts(
                array(
                    'post_type'      => 'category_definer',
                    'fields'         => 'ids',
                    'meta_query' => array(
                        array(
                            'key' => 'select_post_to_show_as_default_option', // name of custom field
                            'value' => '"yes"', // matches exactly "red"
                            'compare' => 'LIKE'
                        )
                    )
                )
            );

            // Get acf value
            foreach ( $post_ids as $id ) {
                $category_definers[$id] = self::prepareCategoryDefinerArray( $id );
            }

        }

        return $category_definers;
    }

    /**
     * Prepare category definer array
     * @param $postId
     * @return array
     */
    public static function prepareCategoryDefinerArray( $postId ) {
        $categoryDefinerConfigurations = get_field( 'category_definer_field_group_configurations', $postId );

        // Return
        return [
            'post_title'     => get_the_title( $postId ),
            'post_excerpt'   => $categoryDefinerConfigurations['title'],
            'post_image'     => $categoryDefinerConfigurations['image']['url'],
        ];
    }

    /**
     * Set messages in session.
     *
     * @param $messages array
     *
     * @return void
     */
    public static function setSessionMessages($messages) {

        // Basics.
        $preparedMessages = [];

        // Iterate all messages.
        foreach ($messages as $type => $messageGroup) {

            // If it has a valid type?
            if (array_key_exists($type, self::$themeMessageConfigurations['types'])) {

                // Iterate group.
                foreach ($messageGroup as $message) {
                    $preparedMessages[$type][] = $message;
                }
            }
        }

        // Set final messages in session.
        if (count($preparedMessages) > 0) {
            $_SESSION[self::$themeMessageConfigurations['sessionName']] = json_encode($preparedMessages);
        }
    }

    /**
     * Get all messages from session.
     *
     * @return array
     */
    public static function getSessionMessages() {

        // Basics.
        $sessionMessagesName = self::$themeMessageConfigurations['sessionName'];

        // Let's get raw messages.
        if (isset($_SESSION[$sessionMessagesName])) {
            $sessionMessages = json_decode($_SESSION[$sessionMessagesName], true);

            // Unset session.
            unset($_SESSION[$sessionMessagesName]);

        } else {
            $sessionMessages = [];
        }

        // Return.
        return $sessionMessages;
    }

    /**
     * Get success messages from session.
     *
     * @return string
     */
    public static function getSessionSuccessMessages() {

        // Basics.
        $preparedMessages = [];
        $rawMessages = self::getSessionMessages();

        // Iterate all messages.
        if (!empty($rawMessages['success'])) {
            foreach ($rawMessages['success'] as $message) {
                 $preparedMessages[] = implode('', [
                     '<p>',
                     $message,
                     '</p>',
                 ]);
            }
        }

        // Return.
        return implode('', $preparedMessages);
    }
}