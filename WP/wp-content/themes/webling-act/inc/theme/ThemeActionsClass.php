<?php

class ThemeActionsClass
{
    /**
     * Class properties.
     *
     */

    /**
     * Constructor.
     *
     */
    public function __construct()
    {
    }

    /**
     * Register category definer default value.
     *
     * @since   1.0.0
     * @return  array
     */
    public static function registerCategoryDefinerDefaultValue( $value, $post_id, $field ) {

        // Loads current post
        $post = get_post( $post_id );

        // Get post meta value for category definers
        $categoryDefinerPostValue = get_post_meta($post_id, 'components_return_points_category_definers', true);

        // Check we are on the new screen
        if ( $post->post_status == 'auto-draft' || empty($categoryDefinerPostValue) ) {

            // Pull in custom post types from fields
            $post_types = $field['post_type'];

            // If post type is category definer
            if ( $post_types[0] == 'category_definer' ) {

                // Fetches all post IDs choices
                $post_ids = get_posts(
                    array(
                        'post_type'      => $post_types,
                        'fields'         => 'ids',
                        'meta_query' => array(
                            array(
                                'key' => 'select_post_to_show_as_default_option', // name of custom field
                                'value' => '"yes"', // matches exactly "red"
                                'compare' => 'LIKE'
                            )
                        )
                    )
                );

                // Assign all choices as the default value
                $value = $post_ids;

            }

        }

        return $value;
    }

    /**
     * Allow additional mime types.
     *
     * @param $mimes array
     *
     * @return array
     */
    public static function registerAdditionalMimeTypes($mimes) {
        $new_mimes = array();
        $new_mimes['svg'] = 'image/svg+xml';
        $mimes = array_merge($mimes, $new_mimes );
        return $mimes;
    }

    /**
     * Register user session.
     *
     * @return void
     */
    public static function registerUserSession() {
        if ( !session_id() ) {
            session_start();
        }
    }

    /**
     * Admin menu access permission for specific users
     *
     * @return void
     */
    public static function registerAdminMenuUserAccess() {
        $user = wp_get_current_user();
        $userPermissions = ThemeConfigurationsClass::$themeConfigurations;
        $userIds = $userPermissions['permissions']['acf']['allowUserIds'];

        // Check for specific user
        if( $user  && !in_array( $user->ID, $userIds )  ) {
            remove_menu_page( 'edit.php?post_type=acf-field-group' );
        }
    }

    /**
     * Register return point data controller
     *
     * @return array
     */
    public static function registerToolkitDataController( $controllers ) {
        $controllers[] = 'toolkitdata';
        return $controllers;
    }

    /**
     * Register json api controller path
     *
     * @return string
     */
    public static function registerJsonApiControllerPath() {
        return get_template_directory() . "/inc/theme/ThemeJsonApiClass.php";
    }

    /**
     * Register toolkit form short code.
     *
     * @param $attributes array
     *
     * @return string
     */
    public static function registerToolkitFormShortCode( $attributes ) {

        // Basic.
        global $post;
        $response = 'No toolkit available';

        // Check if post id exists?
        if( !empty($post) ) {
            $response = AcfToolkitClass::prepareToolkitHtml( $post->ID );
        }

        // Return.
        return $response;
    }

    /**
     * Register after toolkit form submit.
     *
     * @param $confirmation string
     * @param $form
     * @param $entry array
     * @param $ajax
     *
     * @return void
     */
    public static function registerAfterToolkitFormSubmit( $confirmation, $form, $entry, $ajax ) {

        // Basics.
        global $post;

        // Let's get confirmation message.
        $toolkitConfirmationMessage = AcfToolkitClass::getToolkitConfirmation(
            $entry
        );

        // Set message in session.
        ThemeHelpersClass::setSessionMessages([
            'success' => [
                $toolkitConfirmationMessage
            ]
        ]);

        // Set value in session to force user to scroll.
        $_SESSION['webling_toolkit_scroll_bottom'] = true;

        // Let's redirect.
        wp_redirect(
            get_post_permalink($post->ID)
        );
        exit;
    }

    /**
     * Register footer javascript.
     *
     */
    public static function registerFooterJavaScript() {
        ?>
        <script>
            jQuery( document ).ready( function( $ ) {

                // Form confirmation loaded script
                jQuery(document).on("gform_confirmation_loaded", function (e, form_id) {
                    var topSectionHeight = $('.c-live-return-wrapper').innerHeight() + $('.c-inner-header').innerHeight() + 10;
                    $(this).find('.c-download-toolkits__container').each(function(){
                        $('html, body').animate( {
                            scrollTop: $(this).offset().top - topSectionHeight
                        }, '200' );
                    });
                });

                jQuery('.c-show-download-toolkits-form').click(function(){
                    jQuery(this).addClass('u-hidden');
                    jQuery(this).parents('.c-download-toolkits__container')
                        .find('.c-download-toolkits--form').removeClass('u-hidden');
                });
            });

            function CheckPreviousFormSubmitForThisToolkitForm(toolkit_id, toolkit_type, listallcookie, url) {
                jQuery.ajax({
                    type : "post",
                    url: url,
                    async: false,
                    data : {toolkitid: toolkit_id, toolkittype: toolkit_type, listallcookie: listallcookie},
                    success: function(response) {
                        if(response) {
                            // HAVE SOME LOG
                        } else {
                            // HAVE SOME LOG
                        }
                    }
                });
            }
        </script>
        <?php

        // Do we have post request?
        if (!empty($_POST)) {
            ?>
            <script>
                jQuery(document).ready(function ($) {
                    jQuery(document).scrollTop($(".c-download-toolkits__container").offset().top - 140);
                });
            </script>
            <?php
        }

        // Or session set?
        if (isset($_SESSION['webling_toolkit_scroll_bottom'])) {
            ?>
            <script>
                jQuery(document).ready(function ($) {
                    jQuery(document).scrollTop($(".c-download-toolkits--success").offset().top - 140);
                });
            </script>
            <?php

            // Unset if session is set.
            if (isset($_SESSION['webling_toolkit_scroll_bottom'])) {
                unset($_SESSION['webling_toolkit_scroll_bottom']);
            }
        }
    }
}